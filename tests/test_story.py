# -*- coding: utf-8 -*-
#
# Copyright 2018  Ternaris.
# SPDX-License-Identifier: Apache-2.0

# pylint: disable=no-self-use,too-many-public-methods,protected-access

from itertools import count
from mock import PropertyMock, call, patch

import pytest

from grge.project import Project
from grge.story import StoryIssue
from grge.task import Task, TaskIssue


class TestStoryIssue:
    pytestmark = pytest.mark.asyncio

    async def test_tasks(self):
        tasks = [Task('ONE'), Task('TWO')]
        with patch.object(Task, 'extract_all', return_value=tasks) as extract_all:
            story = StoryIssue(None, {'description': 'DESCRIPTION'})
            story.story_project = Project(None, {'id': 23, 'path_with_namespace': 'Story/Project'})
            story.task_project = story.story_project
            assert story.tasks == tuple(x.replace(story=story) for x in tasks)
            extract_all.assert_called_once_with('DESCRIPTION')

    async def test_ignore_foreign_tasks(self):
        tasks = [
            Task('Bad', project_path='foreign'),
            Task('Bad', project_path='Story/Other'),
            Task('Good'),
            Task('Good', project_path='Story/Project'),
        ]
        with patch.object(Task, 'extract_all', return_value=tasks):
            story = StoryIssue(None, {'description': 'DESCRIPTION', 'iid': 42})
            story.story_project = Project(None, {'id': 23, 'path_with_namespace': 'Story/Project'})
            story.task_project = story.story_project
            assert story.tasks == tuple(x.replace(story=story) for x in tasks)[2:]

        tasks = [
            Task('Bad', project_path='Group/Stories'),
            Task('Bad', iid=99),
            Task('Good'),
            Task('Good', project_path='Group/Tasks'),
            Task('Good', project_path='Tasks'),
        ]
        with patch.object(Task, 'extract_all', return_value=tasks):
            story = StoryIssue(None, {'description': 'DESCRIPTION', 'iid': 42})
            story.story_project = Project(None, {'id': 23, 'path_with_namespace': 'Group/Stories'})
            story.task_project = Project(None, {'id': 17, 'path_with_namespace': 'Group/Tasks'})
            assert story.tasks == tuple(x.replace(story=story) for x in tasks)[2:]

    async def test_tasks_description_none(self):
        story = StoryIssue(None, {'description': None})
        story.story_project = Project(None, {'id': 23, 'path_with_namespace': 'Story/Project'})
        story.task_project = story.story_project
        assert story.tasks == ()

    async def test_synchronize_tasks_noop(self):
        with patch.object(StoryIssue, 'tasks', return_value=[]):
            story = StoryIssue(None, {})
            story.story_project = Project(None, {'id': 23, 'path_with_namespace': 'Story/Project'})
            story.task_project = story.story_project
            await story.synchronize_tasks({})

    @pytest.fixture()
    def story(self, mocker):
        story = StoryIssue(None, {
            'iid': 42,
            'title': 'STORY',
            'labels': [],
            'project_id': 23,
            'description': '...\n...\n...\n...',
        })
        story.story_project = Project(None, {'id': 23, 'path_with_namespace': 'Story/Project'})
        story.task_project = story.story_project
        story.put_and_update = mocker.CoroutineMock()
        story.refetch_info = mocker.CoroutineMock()

        tasks = [
            Task('one', story=story, sourcepos=[[2, 2], []]),
            Task('two', story=story, sourcepos=[[4, 2], []]),
        ]

        with patch.object(StoryIssue, 'tasks', new_callable=PropertyMock) as _tasks:
            _tasks.return_value = tasks
            story.mock_tasks = _tasks
            yield story

    @pytest.fixture()
    def task_issue_create(self, mocker):
        iid = count(1)
        user_infos = {
            10: {'id': 10, 'username': 'alice'},
            20: {'id': 20, 'username': 'bob'},
        }

        def _task_issue_create(api, project_id, params):  # pylint: disable=unused-argument
            info = params.copy()
            info['assignees'] = [user_infos[x] for x in info.pop('assignee_ids', [])]
            info['iid'] = next(iid)
            info['state'] = 'opened'
            info['labels'] = sorted(info['labels'])
            return TaskIssue(api, info)

        task_issue_create = mocker.patch.object(TaskIssue, 'create', new=mocker.CoroutineMock())
        task_issue_create.side_effect = _task_issue_create
        yield task_issue_create

    async def test_synchronize_tasks_inactive(self, story):
        await story.synchronize_tasks({})
        story.refetch_info.assert_not_called()
        story.put_and_update.assert_not_called()

    async def test_synchronize_tasks(self, story, task_issue_create):
        """Task issues are created and referenced in the story's task list."""
        story.info['labels'].append('active')

        await story.synchronize_tasks({})

        assert task_issue_create.call_args_list == [
            call(None, 23, {'title': 'one - #42 STORY', 'labels': ('active', 'task')}),
            call(None, 23, {'title': 'two - #42 STORY', 'labels': ('active', 'task')}),
        ]
        story.refetch_info.assert_called_once_with()
        # TODO eol
        story.put_and_update.assert_called_once_with({
            'description': '...\n.[ ] #1 one\n...\n.[ ] #2 two',
        })

    async def test_synchronize_tasks_extra_project(self, story, task_issue_create):
        """Task issues are created and referenced in the story's task list."""
        story.info['labels'].append('active')

        story.task_project = Project(None, {'id': 17, 'path_with_namespace': 'Task/Project'})
        await story.synchronize_tasks({})

        assert task_issue_create.call_args_list == [
            call(None, 17, {'title': 'one - Story/Project#42 STORY', 'labels': ('active', 'task')}),
            call(None, 17, {'title': 'two - Story/Project#42 STORY', 'labels': ('active', 'task')}),
        ]
        story.refetch_info.assert_called_once_with()
        # TODO eol
        story.put_and_update.assert_called_once_with({
            'description': '...\n.[ ] Task/Project#1 one\n...\n.[ ] Task/Project#2 two',
        })

    async def test_synchronize_tasks_extra_project_same_group(self, story, task_issue_create):
        """Task issues are created and referenced in the story's task list."""
        story.info['labels'].append('active')

        story.story_project = Project(None, {'id': 23, 'path_with_namespace': 'Group/Stories'})
        story.task_project = Project(None, {'id': 17, 'path_with_namespace': 'Group/Tasks'})
        await story.synchronize_tasks({})

        assert task_issue_create.call_args_list == [
            call(None, 17, {'title': 'one - Stories#42 STORY', 'labels': ('active', 'task')}),
            call(None, 17, {'title': 'two - Stories#42 STORY', 'labels': ('active', 'task')}),
        ]
        story.refetch_info.assert_called_once_with()
        # TODO eol
        story.put_and_update.assert_called_once_with({
            'description': '...\n.[ ] Tasks#1 one\n...\n.[ ] Tasks#2 two',
        })

    async def test_synchronize_tasks_refetch_changed(self, story, task_issue_create):
        """Task list is not updated if the description changed during processing.

        There is still the small possibility of a race condition (see story.py).
        """
        story.info['labels'].append('active')
        retval = story.refetch_info.return_value

        def _change_description():
            story.info['description'] = 'foo'
            return retval
        story.refetch_info.side_effect = _change_description

        await story.synchronize_tasks({})

        assert task_issue_create.call_args_list == [
            call(None, 23, {'title': 'one - #42 STORY', 'labels': ('active', 'task')}),
            call(None, 23, {'title': 'two - #42 STORY', 'labels': ('active', 'task')}),
        ]
        story.refetch_info.assert_called_once_with()
        story.put_and_update.assert_not_called()

    async def test_synchronize_tasks_all_synced(self, story):
        """No update if tasks and task issues are synchronized already."""
        story.info['labels'].append('active')
        task_issues = {
            1: TaskIssue(None, {
                'title': 'one - #42 STORY',
                'labels': ['active', 'task'],
                'iid': 1,
                'state': 'opened',
                'assignees': [],
            }),
            2: TaskIssue(None, {
                'title': 'two - #42 STORY',
                'labels': ['active', 'task'],
                'iid': 2,
                'state': 'opened',
                'assignees': [],
            }),
        }
        story.mock_tasks.return_value = [
            Task('one', iid=1, story=story, sourcepos=[[2, 2], []]),
            Task('two', iid=2, story=story, sourcepos=[[4, 2], []]),
        ]

        await story.synchronize_tasks(task_issues)

        story.refetch_info.assert_not_called()
        story.put_and_update.assert_not_called()

    async def test_sync_update_issue(self, story, issue_cls, mocker):
        mocker.patch.object(story, '_get_task_changes', return_value=None)
        mocker.patch.object(story, '_get_task_issue_changes', return_value={
            'labels': ['foo'],
        })
        task = Task('Foo', iid=1)
        task_issues = {
            1: TaskIssue(None, {'iid': 1, 'title': 'Foo - #42 STORY'}),
        }
        result = await story._synchronize_task(task, task_issues)
        assert result is None
        issue_cls.put_and_update.assert_called_once_with({'labels': ['foo']})

    async def test_sync_create_issue(self, story, task_issue_create, user_cls):
        story.info['labels'].append('active')
        task = Task('Foo',
                    labels=('foo', 'bar'),
                    assignees=('alice', 'bob'))
        task_issues = {}
        changed_task = await story._synchronize_task(task, task_issues)
        assert changed_task == task.replace(iid=1)
        task_issue_create.assert_called_once_with(None, 23, {
            'labels': ('active', 'task', 'foo', 'bar'),
            'title': 'Foo - #42 STORY',
            'assignee_ids': [10, 20],
        })
        assert user_cls.fetch_by_username.call_args_list == [
            call(None, 'alice'),
            call(None, 'bob'),
        ]

    async def test_sync_create_issue_task_project(self, story, task_issue_create):
        story.info['labels'].append('active')
        story.task_project = Project(None, {'id': 17, 'path_with_namespace': 'Task/Project'})
        task = Task('Foo',
                    project_path='Task/Project',
                    labels=('foo', 'bar'))
        task_issues = {}
        changed_task = await story._synchronize_task(task, task_issues)
        assert changed_task == task.replace(iid=1)
        task_issue_create.assert_called_once_with(None, 17, {
            'labels': ('active', 'task', 'foo', 'bar'),
            'title': 'Foo - Story/Project#42 STORY',
        })

    async def test_sync_create_issue_task_project_same_group(self, story, task_issue_create):
        story.info['labels'].append('active')
        story.story_project = Project(None, {'id': 23, 'path_with_namespace': 'Group/Stories'})
        story.task_project = Project(None, {'id': 17, 'path_with_namespace': 'Group/Tasks'})
        task = Task('Foo',
                    project_path='Tasks',
                    labels=('foo', 'bar'))
        task_issues = {}
        changed_task = await story._synchronize_task(task, task_issues)
        assert changed_task == task.replace(iid=1)
        task_issue_create.assert_called_once_with(None, 17, {
            'labels': ('active', 'task', 'foo', 'bar'),
            'title': 'Foo - Stories#42 STORY',
        })

    async def test_sync_create_issue_inactive(self, story):
        task = Task('Foo')
        task_issues = {}
        changed_task = await story._synchronize_task(task, task_issues)
        assert changed_task is None

    async def test_sync_create_issue_done(self, story):
        task = Task('Foo', done=True)
        task_issues = {}
        changed_task = await story._synchronize_task(task, task_issues)
        assert changed_task is None

    async def test_sync_task_match_by_title(self, story, mocker):
        task = Task('Foo')
        task_issues = {
            1: TaskIssue(None, {
                'iid': 1,
                'title': 'Foo - #42 STORY',
                'assignees': [],
                'labels': [],
            }),
        }
        mocker.patch.object(story, '_get_task_issue_changes', return_value=None)
        changed_task = await story._synchronize_task(task, task_issues)
        assert changed_task == Task('Foo', iid=1)

    async def test_sync_task_match_by_title_different_project(self, story, mocker):
        story.task_project = Project(None, {'id': 17, 'path_with_namespace': 'Task/Project'})
        task = Task('Foo')
        task_issues = {
            1: TaskIssue(None, {
                'iid': 1,
                'title': 'Foo - Story/Project#42 STORY',
                'assignees': [],
                'labels': [],
            }),
        }
        mocker.patch.object(story, '_get_task_issue_changes', return_value=None)
        changed_task = await story._synchronize_task(task, task_issues)
        assert changed_task == Task('Foo', iid=1, project_path='Task/Project')

    async def test_sync_task_match_by_title_different_project_same_group(self, story, issue_cls):
        story.story_project = Project(None, {'id': 23, 'path_with_namespace': 'Group/Stories'})
        story.task_project = Project(None, {'id': 17, 'path_with_namespace': 'Group/Tasks'})
        task = Task('Foo')
        task_issues = {
            1: TaskIssue(None, {
                'iid': 1,
                'title': 'Foo - Stories#42 STORY',
                'assignees': [],
                'labels': [],
            }),
        }
        changed_task = await story._synchronize_task(task, task_issues)
        assert changed_task == Task('Foo', iid=1, project_path='Tasks')
        story.put_and_update.assert_not_called()

        task = Task('Foo')
        task_issues = {
            1: TaskIssue(None, {
                'iid': 1,
                'title': 'Foo - Group/Stories#42 STORY',
                'assignees': [],
                'labels': [],
            }),
        }
        issue_cls.put_and_update.side_effect = task_issues[1].info.update
        changed_task = await story._synchronize_task(task, task_issues)
        assert changed_task == Task('Foo', iid=1, project_path='Tasks')

    async def test_sync_task_match_by_title_fail(self, story):
        task = Task('Foo')
        task_issues = {
            1: TaskIssue(None, {'iid': 1, 'title': 'Bar - #42 STORY'}),
        }
        result = await story._synchronize_task(task, task_issues)
        assert result is None

    async def test_sync_task_skip_vanished(self, story, caplog):
        task = Task('', iid=1)
        task_issues = {}
        result = await story._synchronize_task(task, task_issues)
        assert result is False
        assert caplog.record_tuples == [('root', 30, 'Skipping vanished task #1')]

    @pytest.mark.parametrize('active,task,issue,changes', [
        [None,
         Task(title='Task A'),
         TaskIssue(None, {'title': 'Task A - #42 STORY',  # type: ignore
                          'iid': 1,
                          'labels': ['task'],
                          'assignees': []}),
         {'iid': 1}],
        [None,
         Task(title='Task A', iid=1),
         TaskIssue(None, {'title': 'Task A - #42 STORY',  # type: ignore
                          'iid': 1,
                          'labels': ['task'],
                          'assignees': []}),
         {}],
        [True,
         Task(title='Task A', iid=1),
         TaskIssue(None, {'title': 'Task A - #42 STORY',  # type: ignore
                          'iid': 1,
                          'labels': ['active', 'task'],
                          'state': 'opened',
                          'assignees': []}),
         {}],
        [True,
         Task(title='Task A', iid=1),
         TaskIssue(None, {'title': 'Task A - #42 STORY',  # type: ignore
                          'iid': 1,
                          'labels': ['active', 'task'],
                          'state': 'closed',
                          'assignees': []}),
         {'done': True}],
        [True,
         Task(title='Task A', iid=1, done=True),
         TaskIssue(None, {'title': 'Task A - #42 STORY',  # type: ignore
                          'iid': 1,
                          'labels': ['active', 'task'],
                          'state': 'closed',
                          'assignees': []}),
         {}],
        [True,
         Task(title='Task A', iid=1, done=True),
         TaskIssue(None, {'title': 'Task A - #42 STORY',  # type: ignore
                          'iid': 1,
                          'labels': ['active', 'task'],
                          'state': 'opened',
                          'assignees': []}),
         {'done': False}],
        [True,
         Task(title='Task A', iid=1, assignees=('eve',)),
         TaskIssue(None, {'title': 'Task A Extended - #42 STORY',  # type: ignore
                          'iid': 1,
                          'labels': ['active', 'task', 'foo', 'bar'],
                          'state': 'opened',
                          'assignees': [
                              {'id': 10, 'username': 'alice'},
                              {'id': 20, 'username': 'bob'},
                          ]}),
         {'title': 'Task A Extended',
          'labels': ('bar', 'foo'),
          'assignees': ('alice', 'bob')}],
    ])
    async def test_task_changes(self, story, active, task, issue, changes):
        _changes = story._get_task_changes(active, task, issue)
        assert _changes == changes

    @pytest.mark.parametrize('active,issue,changes', [
        [False,
         TaskIssue(None, {'title': 'Task A - #42 STORY',  # type: ignore
                          'iid': 1,
                          'labels': ['task', 'foo'],
                          'assignees': []}),
         {}],
        [False,
         TaskIssue(None, {'title': 'Task A - #42 STORY',  # type: ignore
                          'iid': 1,
                          'labels': ['active', 'task', 'foo'],
                          'assignees': []}),
         {'labels': ('foo', 'task')}],
        [True,
         TaskIssue(None, {'title': 'Task A - #42 STORY',  # type: ignore
                          'iid': 1,
                          'labels': ['active', 'task', 'foo'],
                          'assignees': []}),
         {}],
        [True,
         TaskIssue(None, {'title': 'Task A - #42 STORY',  # type: ignore
                          'iid': 1,
                          'labels': ['task', 'foo'],
                          'assignees': []}),
         {'labels': ('active', 'foo', 'task')}],
    ])
    async def test_task_issue_changes(self, story, active, issue, changes):
        _changes = story._get_task_issue_changes(active, issue)
        assert _changes == changes
