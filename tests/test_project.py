# -*- coding: utf-8 -*-
#
# Copyright 2019  Ternaris.
# SPDX-License-Identifier: Apache-2.0
#
# This module contains code originally published
# Copyright 2017 Smarkets Limited
# SPDX-License-Identifier: BSD-3-Clause

import pytest

from grge.gitlab import GET, Version
from grge.project import AccessLevel, Project


INFO = {
    'id': 1234,
    'path_with_namespace': 'cool/project',
    'ssh_url_to_repo': 'ssh://blah.com/cool/project.git',
    'merge_requests_enabled': True,
    'only_allow_merge_if_pipeline_succeeds': True,
    'only_allow_merge_if_all_discussions_are_resolved': False,
    'permissions': {
        'project_access': {
            'access_level': AccessLevel.developer.value,
        },
        'group_access': {
            'access_level': AccessLevel.developer.value,
        }
    }
}

GROUP_ACCESS = {
    'project_access': None,
    'group_access': {
        'access_level': AccessLevel.developer.value,
    }
}

NONE_ACCESS = {
    'project_access': None,
    'group_access': None
}


class TestProject:
    pytestmark = pytest.mark.asyncio

    async def test_fetch_by_id(self, make_api):
        api = make_api(call=INFO)

        project = await Project.fetch_by_id(project_id=1234, api=api)

        api.call.assert_called_once_with(GET('/projects/1234'))
        assert project.info == INFO

    async def test_fetch_by_path_exists(self, make_api):
        prj2 = dict(INFO, id=1235, path_with_namespace='foo/bar')
        api = make_api(call=prj2)

        project = await Project.fetch_by_path('foo/bar', api)

        api.call.assert_called_once_with(GET('/projects/foo%2Fbar'))
        assert project and project.info == prj2

    async def test_fetch_all_mine_with_permissions(self, make_api):
        prj1, prj2 = INFO, dict(INFO, id=678)
        api = make_api(collect_all_pages=[prj1, prj2],
                       version=Version.parse("11.0.0-ee"))

        result = await Project.fetch_all_mine(api)
        api.collect_all_pages.assert_called_once_with(GET(
            '/projects',
            {
                'membership': True,
                'with_merge_requests_enabled': True,
            },
        ))
        assert [prj.info for prj in result] == [prj1, prj2]
        assert all(prj.access_level == AccessLevel.developer for prj in result)

    async def test_fetch_all_mine_with_min_access_level(self, make_api):
        prj1 = dict(INFO, permissions=NONE_ACCESS.copy())
        prj2 = dict(INFO, id=678, permissions=NONE_ACCESS.copy())
        api = make_api(collect_all_pages=[prj1, prj2],
                       version=Version.parse("11.2.0-ee"))

        result = await Project.fetch_all_mine(api)
        api.collect_all_pages.assert_called_once_with(GET(
            '/projects',
            {
                'membership': True,
                'with_merge_requests_enabled': True,
                "min_access_level": AccessLevel.developer.value,
            },
        ))
        assert [prj.info for prj in result] == [prj1, prj2]
        assert all(prj.info["permissions"]["marge"] for prj in result)
        assert all(prj.access_level == AccessLevel.developer for prj in result)

    async def test_properties(self):
        project = Project(None, info=INFO)
        assert project.id == 1234
        assert project.path_with_namespace == 'cool/project'
        assert project.ssh_url_to_repo == 'ssh://blah.com/cool/project.git'
        assert project.merge_requests_enabled is True
        assert project.only_allow_merge_if_pipeline_succeeds is True
        assert project.only_allow_merge_if_all_discussions_are_resolved is False
        assert project.access_level == AccessLevel.developer

    async def test_group_access(self):
        project = Project(None, info=dict(INFO, permissions=GROUP_ACCESS))
        bad_project = Project(None, info=dict(INFO, permissions=NONE_ACCESS))
        assert project.access_level == AccessLevel.developer
        with pytest.raises(AssertionError):
            bad_project.access_level  # pylint: disable=pointless-statement
