# -*- coding: utf-8 -*-
#
# Copyright 2019  Ternaris.
# SPDX-License-Identifier: Apache-2.0

# pylint: disable=no-self-use

from mock import call

import pytest

from grge.config import ProjectSyncSource, ProjectSyncTarget, SyncExternalIssues
from grge.issue import Issue
from grge.sync_external_issues import BrokenTargetIssue, ExternalIssuesSyncer


SOURCE1 = {
    'gitlab_url': 'http://SOURCE',
    'api_token': 'TOKEN',
    'project_path': 'GROUP/PROJECT',
}
SOURCE2 = {
    'gitlab_url': '',
    'api_token': '',
    'project_path': '',
}

TARGET = {
    'gitlab_url': 'http://TARGET',
    'api_token': 'TOKEN',
    'project_path': 'TARGET_PROJECT',
}

CFG = SyncExternalIssues.from_dict({
    'sources': [SOURCE1],
    'target': TARGET,
})
CFG2 = SyncExternalIssues.from_dict({
    'sources': [SOURCE1, SOURCE2],
    'target': TARGET,
})

ALICE = {
    'username': 'alice',
    'web_url': 'ALICE_WEB_URL'
}
BOB = {
    'username': 'bob',
    'web_url': 'BOB_WEB_URL',
}

ISSUE1 = Issue(None, {  # type: ignore
    'iid': 1,
    'author': ALICE,
    'assignees': (),
    'labels': (),
    'title': 'ISSUE 1',
    'web_url': 'http://SOURCE/GROUP/PROJECT/issues/1',
})
ISSUE2 = Issue(None, {  # type: ignore
    'iid': 2,
    'author': BOB,
    'assignees': (ALICE,),
    'labels': (),
    'title': 'ISSUE 2',
    'web_url': 'http://SOURCE/GROUP/PROJECT/issues/2',
})

TARGET_ISSUE1 = Issue(None, {  # type: ignore
    'title': ISSUE1.title,
    'description': (
        f'- upstream: {ISSUE1.web_url}\n'
        f'  - author: [alice](ALICE_WEB_URL)\n'
        f'  - assignees: \n'
        f'  - labels: \n'
    ),
    'labels': ('managed',),
    'state': 'opened',
})
TARGET_ISSUE2 = Issue(None, {  # type: ignore
    'title': ISSUE2.title,
    'description': (
        f'- upstream: {ISSUE2.web_url}\n'
        f'  - author: [bob](BOB_WEB_URL)\n'
        f'  - assignees: [alice](ALICE_WEB_URL)\n'
        f'  - labels: \n'
    ),
    'labels': ('managed',),
    'state': 'opened',
})


def create_params_from_issue(issue, **kw):
    return dict({
        'title': issue.title,
        'description': issue.description,
        'labels': issue.labels,
    }, **kw)


class TestSyncExternalIssues:
    pytestmark = pytest.mark.asyncio

    @pytest.fixture
    def fetch_source(self, mocker):
        return mocker.patch('grge.sync_external_issues.ExternalIssuesSyncer.fetch_source_issues',
                            new=mocker.CoroutineMock())

    @pytest.fixture
    def fetch_target(self, mocker):
        return mocker.patch('grge.sync_external_issues.ExternalIssuesSyncer.fetch_target_issues',
                            new=mocker.CoroutineMock())

    @pytest.fixture
    def create(self, mocker):
        return mocker.patch('grge.issue.Issue.create', new=mocker.CoroutineMock())

    @pytest.fixture
    def put(self, mocker):
        return mocker.patch('grge.issue.Issue.put_and_update', new=mocker.CoroutineMock())

    async def test_empty(self, fetch_target, fetch_source, create):
        fetch_source.return_value = []
        fetch_target.return_value = []
        syncer = ExternalIssuesSyncer(CFG2)
        await syncer.run()
        fetch_target.assert_called_once_with(CFG2.target)
        assert fetch_source.call_args_list == [call(x) for x in CFG2.sources]
        create.assert_not_called()

    async def test_no_target_issues_exist(self, mocker, fetch_target, fetch_source, create):
        mocker.patch('grge.gitlab.get_api', return_value=None)
        fetch_source.return_value = [ISSUE1, ISSUE2]
        fetch_target.return_value = []
        syncer = ExternalIssuesSyncer(CFG)
        await syncer.run()
        assert create.call_args_list == [
            call(None, 'TARGET_PROJECT', create_params_from_issue(TARGET_ISSUE1)),
            call(None, 'TARGET_PROJECT', create_params_from_issue(TARGET_ISSUE2)),
        ]

    async def test_one_target_issue_exists(self, mocker, fetch_target, fetch_source, create):
        mocker.patch('grge.gitlab.get_api', return_value=None)
        fetch_source.return_value = [ISSUE1, ISSUE2]
        fetch_target.return_value = [TARGET_ISSUE1]
        syncer = ExternalIssuesSyncer(CFG)
        await syncer.run()
        assert create.call_args_list == [
            call(None, 'TARGET_PROJECT', create_params_from_issue(TARGET_ISSUE2)),
        ]

    async def test_broken_preamble_aborts(self, fetch_target, fetch_source, create):
        fetch_source.return_value = [ISSUE1, ISSUE2]
        broken_target_issue = Issue(None, dict(TARGET_ISSUE1.info,
                                               description='',
                                               web_url='BROKEN_WEB_URL'))
        fetch_target.return_value = [broken_target_issue]
        syncer = ExternalIssuesSyncer(CFG)
        with pytest.raises(BrokenTargetIssue) as e_info:
            await syncer.run()
        assert 'BROKEN_WEB_URL' in str(e_info.value)
        create.assert_not_called()

    async def test_all_target_issues_exist(self, fetch_target, fetch_source, create):
        fetch_source.return_value = [ISSUE1, ISSUE2]
        fetch_target.return_value = [TARGET_ISSUE1, TARGET_ISSUE2]
        syncer = ExternalIssuesSyncer(CFG)
        await syncer.run()
        create.assert_not_called()

    async def test_different_managed_label(self, mocker, fetch_target, fetch_source, create):
        mocker.patch('grge.gitlab.get_api', return_value=None)
        cfg = SyncExternalIssues.from_dict({
            'sources': [SOURCE1],
            'target': dict(TARGET, managed_label='MAN'),
        })
        fetch_source.return_value = [ISSUE1]
        fetch_target.return_value = []
        syncer = ExternalIssuesSyncer(cfg)
        await syncer.run()
        assert create.call_args_list == [
            call(None, 'TARGET_PROJECT', create_params_from_issue(TARGET_ISSUE1, labels=('MAN',))),
        ]

    async def test_update(self, put, fetch_target, fetch_source, create):
        cfg = SyncExternalIssues.from_dict({
            'sources': [
                dict(SOURCE1, static_labels=['foo', 'bar'], label_map={'a': 'x', 'b': 'y'}),
            ],
            'target': TARGET,
        })
        source = Issue(None, dict(ISSUE1.info, labels=('y', 'z')))
        target = Issue(None, dict(TARGET_ISSUE1.info,
                                  labels=('keep', 'a', 'managed'),
                                  state='closed'))
        fetch_source.return_value = [source]
        fetch_target.return_value = [target]
        syncer = ExternalIssuesSyncer(cfg)
        await syncer.run()
        create.assert_not_called()
        put.assert_called_once_with({
            'labels': {'managed', 'b', 'bar', 'keep', 'foo'},
            'state_event': 'reopen',
            'description': (
                f'- upstream: {source.web_url}\n'
                f'  - author: [alice](ALICE_WEB_URL)\n'
                f'  - assignees: \n'
                f'  - labels: y, z\n\n'
            ),
        })

    async def test_close(self, put, fetch_target, fetch_source, create):
        fetch_source.return_value = []
        fetch_target.return_value = [
            TARGET_ISSUE1,
            Issue(None, dict(TARGET_ISSUE2.info, state='closed'))
        ]
        syncer = ExternalIssuesSyncer(CFG)
        await syncer.run()
        create.assert_not_called()
        put.assert_called_once_with({
            'state_event': 'close',
        })


class TestFetchIssues:
    pytestmark = pytest.mark.asyncio

    async def test_fetch_source_issues(self, issue_search_project, mocker):
        api = mocker.patch('grge.gitlab.get_api', return_value='API')
        await ExternalIssuesSyncer.fetch_source_issues(ProjectSyncSource(
            gitlab_url='GITLAB',
            api_token='TOKEN',
            project_path='PATH',
        ))
        api.assert_called_once_with('GITLAB', 'TOKEN')
        issue_search_project.assert_called_once_with('API', 'PATH', {'state': 'opened'})

    async def test_fetch_target_issues(self, issue_search_project, mocker):
        api = mocker.patch('grge.gitlab.get_api', return_value='API')
        await ExternalIssuesSyncer.fetch_target_issues(ProjectSyncTarget(
            gitlab_url='GITLAB',
            api_token='TOKEN',
            project_path='PATH',
            managed_label='foo',
        ))
        api.assert_called_once_with('GITLAB', 'TOKEN')
        issue_search_project.assert_called_once_with('API', 'PATH', {'labels': 'foo'})
