# -*- coding: utf-8 -*-
#
# Copyright 2018  Ternaris.
# SPDX-License-Identifier: Apache-2.0

# pylint: disable=no-self-use

from inspect import cleandoc
from mock import Mock

from grge.task import Task


class TestTask:
    def test_extract_all(self):
        document = cleandoc("""

        # Section levels don't matter

        - [ ] ignored

        ## Tasks

        The first Tasks section is relevant.

          - not a task
          - [ ] Todo
            - [ ] Subtask, not handled yet
          - [ ] Multi-line
            task, not handled, yet
         - [x] #1234 Completed task
          - ~~[x] #5678 Ignored~~
          -   [ ]   Task with additional spaces
         - [ ] Group/Project#1234 Task in different project
         - [ ] Project#1234 Not correct but ok
         - [ ] Group/subgroup/project#1234 Project in subgroup
         - [ ] with labels ~foo ~bar
         - [ ] with assignees @alice @bob
         - [ ] with assignees and labels @al.i-c_e @bob ~"foo:bar" ~bar

        ## Tasks

        Any sections after first Tasks section are ignored.

        - [ ] ignored task

        """)

        tasks = Task.extract_all(document)
        assert tasks == [
            Task(title='Todo', iid=None, sourcepos=[[10, 5], [10, 12]]),
            Task(title='Completed task', done=True, iid=1234, sourcepos=[[14, 4], [14, 27]]),
            Task(title='Task with additional spaces', sourcepos=[[16, 7], [16, 39]]),
            Task(title='Task in different project', iid=1234, sourcepos=[[17, 4], [17, 51]],
                 project_path='Group/Project'),
            Task(title='Not correct but ok', iid=1234, sourcepos=[[18, 4], [18, 38]],
                 project_path='Project'),
            Task(title='Project in subgroup', iid=1234, sourcepos=[[19, 4], [19, 54]],
                 project_path='Group/subgroup/project'),
            Task(title='with labels', sourcepos=[[20, 4], [20, 28]],
                 labels=('foo', 'bar')),
            Task(title='with assignees', sourcepos=[[21, 4], [21, 33]],
                 assignees=('alice', 'bob')),
            Task(title='with assignees and labels', sourcepos=[[22, 4], [22, 63]],
                 assignees=('al.i-c_e', 'bob'),
                 labels=('foo:bar', 'bar')),
        ]

        assert Task.extract_all('') == []

    def test_clone(self):
        task = Task(title='TASK').replace(iid=4201)
        assert task.iid == 4201
        assert task.title == 'TASK'

    def test_attributes(self):
        story = Mock(spec=[])
        task = Task(title='TITLE')
        assert task.title == 'TITLE'
        task = Task(title='TITLE', done=True, iid=4201, sourcepos=[[1, 1], [1, 2]], story=story)
        assert task.title == 'TITLE'
        assert task.iid == 4201
        assert task.sourcepos == [[1, 1], [1, 2]]
        assert task.story == story

    def test_render(self):
        assert Task(title='TITLE').render() == '[ ] TITLE'
        assert Task(title='TITLE').render() == '[ ] TITLE'
        assert Task(title='TITLE', done=True).render() == '[x] TITLE'
        assert Task(title='TITLE', iid=4201).render() == '[ ] #4201 TITLE'
        assert Task(title='TITLE', done=True, iid=4201).render() == '[x] #4201 TITLE'
        assert Task(title='TITLE', done=True, iid=4201, project_path='Group/Project').render() == \
            '[x] Group/Project#4201 TITLE'
        assert Task(title='TITLE', done=True, iid=4201, project_path='Group/Project',
                    assignees=('bob', 'alice'), labels=('foo:bar', 'bar')).render() == \
            '[x] Group/Project#4201 TITLE @alice @bob ~bar ~"foo:bar"'
