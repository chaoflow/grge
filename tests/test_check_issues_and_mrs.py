# -*- coding: utf-8 -*-
#
# Copyright 2019  Ternaris.
# SPDX-License-Identifier: Apache-2.0

# pylint: disable=no-self-use,protected-access

import pytest

from mock import call

from grge import config
from grge.issue import Issue
from grge.check_issues_and_mrs import Checker as _Checker
from grge.check_issues_and_mrs import ProjectIssueChecker, GroupIssueChecker
from grge.check_issues_and_mrs import ProjectMergeRequestChecker, GroupMergeRequestChecker
from grge.check_issues_and_mrs import LabelSetCheck
from grge.check_issues_and_mrs import AddLabelsAction, RemoveLabelsAction  # , CommentAction
from grge.check_issues_and_mrs import AssignAction, ReassignAction  # , SetMilestoneAction


GROUP = {
    'gitlab_url': '',
    'api_token': '',
    'group_path': 'GROUP_PATH',
    'rules': [],
}
PROJECT = {
    'gitlab_url': '',
    'api_token': '',
    'project_path': 'PROJECT_PATH',
    'rules': [],
}


class TestCheckerFetches:
    pytestmark = pytest.mark.asyncio

    async def test_project_issue_checker(self, api, issue_search_project):
        issue_search_project.return_value = [1, 2]
        cfg = config.CheckProjectIssues.from_dict(PROJECT)
        assert [x async for x in ProjectIssueChecker(cfg)._fetch()] == [1, 2]
        issue_search_project.assert_called_once_with(api, cfg.project_path)

    async def test_group_issue_checker(self, api, issue_search_group):
        issue_search_group.return_value = [1, 2]
        cfg = config.CheckGroupIssues.from_dict(GROUP)
        assert [x async for x in GroupIssueChecker(cfg)._fetch()] == [1, 2]
        issue_search_group.assert_called_once_with(api, cfg.group_path)

    async def test_project_merge_request_checker(self, api, merge_request_search_project):
        merge_request_search_project.return_value = [1, 2]
        cfg = config.CheckProjectMergeRequests.from_dict(PROJECT)
        assert [x async for x in ProjectMergeRequestChecker(cfg)._fetch()] == [1, 2]
        merge_request_search_project.assert_called_once_with(api, cfg.project_path)

    async def test_group_merge_request_checker(self, api, merge_request_search_group):
        merge_request_search_group.return_value = [1, 2]
        cfg = config.CheckGroupMergeRequests.from_dict(GROUP)
        assert [x async for x in GroupMergeRequestChecker(cfg)._fetch()] == [1, 2]
        merge_request_search_group.assert_called_once_with(api, cfg.group_path)


class Checker(_Checker):
    mock_items = None  # Needs to be overriden in test to be usable

    async def _fetch(self):
        for item in self.mock_items:  # pylint: disable=not-an-iterable
            yield item


class TestChecker:
    pytestmark = pytest.mark.asyncio

    async def test_integration(self, issue_cls, mocker):
        _ = issue_cls
        issue1 = Issue(None, {
            'iid': 1,
            'labels': ['foo'],
            'author': {
                'username': 'AUTHOR',
            },
        })
        issue2 = Issue(None, {
            'iid': 2,
            'labels': ['foo', 'warning'],
        })
        cfg = config.Checker.from_dict({
            'rules': [
                {'filter': '!~warning ~foo',
                 'checks': [
                     {'label_set': ['a', 'b'],
                      'required': True},
                 ],
                 'actions': [
                     {'add_labels': ['warning']},
                     {'comment': {'mention': ['root', '{item.author}']}},
                 ]},
            ],
        })
        put_and_update1 = mocker.patch.object(
            issue1, 'put_and_update', new=mocker.CoroutineMock()
        )
        put_and_update2 = mocker.patch.object(
            issue2, 'put_and_update', new=mocker.CoroutineMock()
        )
        comment1 = mocker.patch.object(
            issue1, 'comment', new=mocker.CoroutineMock()
        )
        comment2 = mocker.patch.object(
            issue2, 'comment', new=mocker.CoroutineMock()
        )
        checker = Checker(cfg)
        checker.mock_items = (issue1, issue2)
        await checker.run()
        assert put_and_update1.call_args_list == [
            call({'labels': frozenset({'foo', 'warning'})}),
        ]
        assert comment1.call_args_list == [
            call('@root @AUTHOR One of ~a ~b is required!'),
        ]
        assert put_and_update2.call_args_list == []
        assert comment2.call_args_list == []

    async def test_label_set_check(self):
        cfg = config.LabelSetCheck.from_dict({
            'label_set': ['a', 'b'],
        })
        check = LabelSetCheck(cfg)
        result = check.run(Issue(None, {'labels': ['a', 'b']}))
        assert result == 'Only one of ~a ~b allowed!'

        result = check.run(Issue(None, {'labels': ['a']}))
        assert result is None

        result = check.run(Issue(None, {'labels': ['b']}))
        assert result is None

        result = check.run(Issue(None, {'labels': []}))
        assert result == 'One of ~a ~b is required!'

        cfg = config.LabelSetCheck.from_dict({
            'label_set': ['a', 'b'],
            'required': False,
        })
        check = LabelSetCheck(cfg)
        result = check.run(Issue(None, {'labels': []}))
        assert result is None

    async def test_add_labels_action(self, issue_cls):
        cfg = config.AddLabelsAction.from_dict({
            'add_labels': ['a', 'b', 'c'],
        })
        action = AddLabelsAction(cfg)
        await action(Issue(None, {'labels': ['c', 'd']}))
        issue_cls.put_and_update.assert_called_once_with(
            {'labels': frozenset({'a', 'b', 'c', 'd'})},
        )

    async def test_add_labels_action_noop(self, issue_cls):
        cfg = config.AddLabelsAction.from_dict({
            'add_labels': ['a', 'b', 'c'],
        })
        action = AddLabelsAction(cfg)
        await action(Issue(None, {'labels': ['a', 'b', 'c', 'd']}))
        issue_cls.put_and_update.assert_not_called()

        issue = Issue(None, {'labels': []})

        def update_labels():
            issue.info['labels'].extend(['a', 'b', 'c', 'd'])

        issue.refetch_info.side_effect = update_labels
        await action(issue)
        issue_cls.put_and_update.assert_not_called()

    async def test_add_labels_refetch_new(self, issue_cls):
        cfg = config.AddLabelsAction.from_dict({
            'add_labels': ['a', 'b', 'c'],
        })
        action = AddLabelsAction(cfg)
        issue = Issue(None, {'labels': []})

        def update_labels():
            issue.info['labels'].extend(['b', 'c', 'd'])

        issue.refetch_info.side_effect = update_labels
        await action(issue)
        issue_cls.put_and_update.assert_called_once_with(
            {'labels': frozenset({'a', 'b', 'c', 'd'})},
        )

    async def test_remove_labels_action(self, issue_cls):
        cfg = config.RemoveLabelsAction.from_dict({
            'remove_labels': ['a', 'b', 'c'],
        })
        action = RemoveLabelsAction(cfg)
        await action(Issue(None, {'labels': ['b', 'c', 'd']}))
        issue_cls.put_and_update.assert_called_once_with(
            {'labels': frozenset({'d'})},
        )

    async def test_remove_labels_action_noop(self, issue_cls):
        cfg = config.RemoveLabelsAction.from_dict({
            'remove_labels': ['a', 'b', 'c'],
        })
        action = RemoveLabelsAction(cfg)
        await action(Issue(None, {'labels': ['d']}))
        issue_cls.put_and_update.assert_not_called()

        issue = Issue(None, {'labels': ['a']})

        def update_labels():
            issue.info['labels'].remove('a')

        issue.refetch_info.side_effect = update_labels
        await action(issue)
        issue_cls.put_and_update.assert_not_called()

    async def test_remove_labels_refetch_new(self, issue_cls):
        cfg = config.RemoveLabelsAction.from_dict({
            'remove_labels': ['a', 'b', 'c'],
        })
        action = RemoveLabelsAction(cfg)
        issue = Issue(None, {'labels': ['a']})

        def update_labels():
            issue.info['labels'].extend(['a', 'b', 'c', 'd'])

        issue.refetch_info.side_effect = update_labels
        await action(issue)
        issue_cls.put_and_update.assert_called_once_with(
            {'labels': frozenset({'d'})},
        )

    @pytest.mark.parametrize('action_cls,cfg', [
        (AssignAction, config.AssignAction.from_dict({
            'assign': ['a', 'b', 'c', '{item.author}'],
        })),
        (ReassignAction, config.ReassignAction.from_dict({
            'reassign': ['a', 'b', 'c', '{item.author}'],
        })),
    ])
    async def test_assign_actions(self, action_cls, cfg, issue_cls, user_cls):
        def fetch_user(self, username):
            _ = self
            users = {
                'a': user_cls(None, {'id': 1}),
                'b': user_cls(None, {'id': 2}),
                'AUTHOR': user_cls(None, {'id': 99}),
            }
            return users[username]

        user_cls.fetch_by_username.side_effect = fetch_user

        action = action_cls(cfg)
        issue = Issue(None, {
            'assignees': [
                {'id': 3, 'username': 'c'},
            ],
            'author': {'id': 99, 'username': 'AUTHOR'},
        })

        def update():
            issue.info['assignees'].extend([
                {'id': 3, 'username': 'c'},
                {'id': 4, 'username': 'd'},
            ])

        issue.refetch_info.side_effect = update
        await action(issue)
        issue_cls.put_and_update.assert_called_once_with(
            ({'assignee_ids': [1, 2, 3, 4, 99]} if action_cls is AssignAction else
             {'assignee_ids': [1, 2, 3, 99]}),
        )

    @pytest.mark.parametrize('action_cls,cfg', [
        (AssignAction, config.AssignAction.from_dict({
            'assign': ['a', 'b'],
        })),
        (ReassignAction, config.ReassignAction.from_dict({
            'reassign': ['a', 'b'],
        })),
    ])
    async def test_assign_action_noop(self, action_cls, cfg, issue_cls, user_cls):
        def fetch_user(self, username):
            _ = self
            users = {
                'a': user_cls(None, {'id': 1}),
                'b': user_cls(None, {'id': 2}),
            }
            return users[username]

        user_cls.fetch_by_username.side_effect = fetch_user
        action = action_cls(cfg)
        issue = Issue(None, {
            'assignees': [
                {'id': 1, 'username': 'a'},
                {'id': 2, 'username': 'b'},
            ]
        })
        await action(issue)
        issue_cls.put_and_update.assert_not_called()

        if action_cls is AssignAction:
            issue = Issue(None, {
                'assignees': []
            })

            def update():
                issue.info['assignees'].extend([
                    {'id': 1, 'username': 'a'},
                    {'id': 2, 'username': 'b'},
                ])

            issue.refetch_info.side_effect = update
            await action(issue)
            issue_cls.put_and_update.assert_not_called()
