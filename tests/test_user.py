# -*- coding: utf-8 -*-
#
# Copyright 2019  Ternaris.
# SPDX-License-Identifier: Apache-2.0
#
# This module contains code originally published
# Copyright 2017 Smarkets Limited
# SPDX-License-Identifier: BSD-3-Clause

# pylint: disable=no-self-use

import pytest
from mock import ANY

from grge.gitlab import GET
from grge.user import User


INFO = {
    'id': 1234,
    'username': 'john_smith',
    'name': 'John Smith',
    'state': 'active',
    'is_admin': False,
}


class TestProjectWithUser:
    pytestmark = pytest.mark.asyncio

    @pytest.fixture
    def api(self, api):
        api.call.return_value = INFO
        return api

    async def test_fetch_myself(self, api):
        await User.myself(api)
        api.call.assert_called_once_with(GET('/user'))

    async def test_fetch_by_id(self, api):
        user = await User.fetch_by_id(api, user_id=1234)
        api.call.assert_called_once_with(GET('/users/1234'))
        assert user.info == INFO

    async def test_fetch_by_username_exists(self, api):
        user = await User.fetch_by_username(api, 'john_smith')
        api.call.assert_called_once_with(GET('/users', {'username': 'john_smith'}, ANY))
        assert user and user.info == INFO

    async def test_properties(self, api):
        user = User(api, info=INFO)
        assert user.id == 1234
        assert user.username == 'john_smith'
        assert user.name == 'John Smith'
        assert user.state == 'active'
