# -*- coding: utf-8 -*-
#
# Copyright 2019  Ternaris.
# SPDX-License-Identifier: Apache-2.0

from collections import namedtuple

import pytest

from grge.markdown import extract_preamble_and_rest

Param = namedtuple('Param', 'description preamble rest')
PARAMS = (
    Param('', '', ''),
    Param('preamble', 'preamble', ''),
    Param('## Subtasks', '', '## Subtasks'),
    Param('preamble\n\n## Subtasks', 'preamble\n\n', '## Subtasks'),
)


@pytest.mark.parametrize('test_set', PARAMS)
def test(test_set):
    preamble, rest = extract_preamble_and_rest(test_set.description)
    assert preamble == test_set.preamble
    assert rest == test_set.rest
