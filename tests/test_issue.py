# -*- coding: utf-8 -*-
#
# Copyright 2018-2019  Ternaris.
# SPDX-License-Identifier: Apache-2.0

# pylint: disable=no-self-use

import pytest

from grge.issue import Issue
from grge.gitlab import POST, PUT, GET


class TestIssue:
    pytestmark = pytest.mark.asyncio

    async def test_create(self, make_api):
        params = {'foo': 1}
        info = {'bar': 2}
        api = make_api(call=info)
        issue = await Issue.create(api, 23, params)
        api.call.assert_called_once_with(POST(endpoint='/projects/23/issues', args={'foo': 1}))
        assert issue.info == info

    async def test_fetch_by_iid(self, make_api):
        info = {'foo': 1}
        api = make_api(call=info)
        issue = await Issue.fetch_by_iid(api, 23, 42)
        api.call.assert_called_once_with(GET(endpoint='/projects/23/issues/42'))
        assert issue.info == info

    async def test_search(self, make_api):
        infos = [{'foo': 1}, {'bar': 2}]
        api = make_api(collect_all_pages=infos)
        issues = await Issue.search_project(api, 23, {'key': 'val'})
        api.collect_all_pages.assert_called_once_with(GET('/projects/23/issues', {'key': 'val'}))
        assert all(x.info == y for x, y in zip(issues, infos))

    async def test_properties(self):
        info = {
            'iid': 42,
            'description': 'DESCRIPTION',
            'labels': ['foo', 'bar'],
            'project_id': 23,
            'state': 'opened',
            'title': 'TITLE',
        }
        issue = Issue(None, info)
        assert issue.info == info
        assert issue.iid == info['iid']
        assert issue.description == info['description']
        assert issue.labels == tuple(info['labels'])
        assert issue.project_id == info['project_id']
        assert issue.state == info['state']
        assert issue.title == info['title']

    async def test_repr(self):
        class SubIssue(Issue):
            pass
        assert repr(SubIssue(None, {'iid': 42})) == '<SubIssue #42>'

    async def test_equality(self):
        assert Issue(None, {'foo': 1}) == Issue(None, {'foo': 1})
        assert Issue(None, {'foo': 1}) != Issue(None, {'foo': 2})

    async def test_put_and_update(self, make_api):
        info = {
            'project_id': 23,
            'iid': 42,
        }
        api = make_api(call=info)
        issue = Issue(api, info.copy())
        changes = {
            'foo': 1
        }
        info.update(changes)
        await issue.put_and_update(changes)
        api.call.assert_called_once_with(PUT(endpoint='/projects/23/issues/42', args={'foo': 1}))
        assert issue.info == info
