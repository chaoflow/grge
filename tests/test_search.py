# -*- coding: utf-8 -*-
#
# Copyright 2019  Ternaris.
# SPDX-License-Identifier: Apache-2.0

import pytest

from grge.issue import Issue
from grge.search import InvalidCharacter, parse_filter
from grge.search import FString, Regex
from grge.search import Negate, MatchAll, MatchAny
from grge.search import Author, Assignee, Label, Milestone, State


@pytest.mark.parametrize('filter_string,expected_filter', (
    ['', MatchAll(())],
    [' ', MatchAll(())],
    ['  ~foo  ', Label('foo', (3, 6))],
    ['~"foo bar"', Label('foo bar', (1, 10))],
    ['!~"foo bar"', Negate(Label('foo bar', (2, 11)), (1, 11))],
    ['~foo  !~bar', MatchAll(
        (
            Label('foo', (1, 4)),
            Negate(Label('bar', (8, 11)), (7, 11))
        ),
        sourcepos=(1, 11)
    )],
    ['~grge:warning', Label('grge:warning', (1, 13))],
    [':state:closed', State('closed', (1, 13))],
    ['%123 !%123 %foo %"Foo bar" %/^foo(.*)/', MatchAll(
        (
            Milestone('123', (1, 4)),
            Negate(Milestone('123', (7, 10)), (6, 10)),
            Milestone('foo', (12, 15)),
            Milestone('Foo bar', (17, 26)),
            Milestone(Regex('^foo(.*)'), (28, 38)),
        ),
        sourcepos=(1, 38),
    )],
    [':assignee:foo :assignee:"foo bar" :assignee:{item.author}', MatchAll(
        (
            Assignee('foo', (1, 13)),
            Assignee('foo bar', (15, 33)),
            Assignee(FString('item.author'), (35, 57)),
        ),
        sourcepos=(1, 57),
    )],
    [':author:foo', Author('foo', (1, 11))],
    ['(any ~foo ~bar)', MatchAny(
        (
            Label('foo', sourcepos=(6, 9)),
            Label('bar', sourcepos=(11, 14)),
        ),
        sourcepos=(1, 15),
    )],
    ['(all ~foo ~bar)', MatchAll(
        (
            Label('foo', sourcepos=(6, 9)),
            Label('bar', sourcepos=(11, 14)),
        ),
        sourcepos=(1, 15),
    )],
    ['(any (all ~foo ~bar) ~xyz)', MatchAny(
        (
            MatchAll((Label('foo', sourcepos=(11, 14)),
                      Label('bar', sourcepos=(16, 19))),
                     sourcepos=(6, 20)),
            Label('xyz', sourcepos=(22, 25)),
        ),
        sourcepos=(1, 26),
    )],
    ['(all ~xyz (any ~foo ~bar))', MatchAll(
        (
            Label('xyz', sourcepos=(6, 9)),
            MatchAny((Label('foo', sourcepos=(16, 19)),
                      Label('bar', sourcepos=(21, 24))),
                     sourcepos=(11, 25)),
        ),
        sourcepos=(1, 26),
    )],
    ['~xyz (any ~foo ~bar)', MatchAll(
        (
            Label(value='xyz', sourcepos=(1, 4)),
            MatchAny((Label(value='foo', sourcepos=(11, 14)),
                      Label(value='bar', sourcepos=(16, 19))),
                     sourcepos=(6, 20)),
        ),
        sourcepos=(1, 20),
    )],
))
def test_parse_filter(filter_string, expected_filter):
    parsed = parse_filter(filter_string)
    assert parsed == expected_filter
    if parsed.sourcepos:
        assert parsed.sourcepos[0] == len(filter_string) - len(filter_string.lstrip()) + 1
        assert parsed.sourcepos[1] == len(filter_string.rstrip())


ITEM = Issue(None, {  # type: ignore
    'iid': 23,
    'labels': ('foo', 'bar'),
    'author': {'username': 'alice', 'id': 1},
    'assignees': [
        {'username': 'alice', 'id': 1},
        {'username': 'bob', 'id': 2},
    ],
    'state': 'opened',
    'milestone': {
        'iid': 42,
        'title': 'Milestone 42',
    },
})


def test_author():
    assert Author('alice')(ITEM)
    assert not Author('bob')(ITEM)


def test_assignee():
    assert Assignee('alice')(ITEM)
    assert Assignee('bob')(ITEM)
    assert not Assignee('eve')(ITEM)


def test_label():
    assert Label('foo')(ITEM)
    assert Label('bar')(ITEM)
    assert not Label('xyz')(ITEM)

    assert not Negate(Label('foo'))(ITEM)
    assert Negate(Negate(Label('foo')))(ITEM)


def test_milestone():
    assert Milestone('42')(ITEM)
    assert not Milestone('23')(ITEM)
    assert Milestone('Milestone 42')(ITEM)
    assert not Milestone('Some other')(ITEM)


def test_state():
    assert State('opened')(ITEM)
    assert not State('closed')(ITEM)


def test_negate():
    assert Negate(Label('xyz'))(ITEM)
    assert not Negate(Label('foo'))(ITEM)


def test_all():
    assert MatchAll((Label('foo'), Label('bar')))(ITEM)
    assert not MatchAll((Label('foo'), Label('xyz')))(ITEM)


def test_any(mocker):
    spy = mocker.spy(Label, '__call__')
    first = Label('xyz')
    second = Label('foo')
    assert MatchAny((first, second))(ITEM)
    assert spy.call_count == 2

    assert not MatchAny((Label('xyz'),))(ITEM)


def test_any_shortcircuit(mocker):
    spy = mocker.spy(Label, '__call__')
    first = Label('foo')
    second = Label('xyz')
    assert MatchAny((first, second))(ITEM)
    assert spy.call_count == 1


def test_invalid_char():
    with pytest.raises(InvalidCharacter):
        parse_filter('* abc')

    with pytest.raises(InvalidCharacter):
        parse_filter(':1foo:')

    with pytest.raises(InvalidCharacter):
        parse_filter(':label:*')

    with pytest.raises(InvalidCharacter):
        parse_filter('(1foo bar)')
