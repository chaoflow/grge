==================
Gitlab Rocket GEar
==================

Gitlab Rocket GEar, abbrev. GRGE or grge, pronounced like George [dʒɔːdʒ].

Grge is a daemon and command line utility augmenting GitLab. The
following functionality is available:

- Synchronization of story and task issues
- Synchronization of external issues
- Actions on issues and merge requests

Run daemon with::

  grge daemon grge-daemon.yaml

See below for example configurations and `config.py
<./grge/config.py>`_ for all details.


Synchronize stories and tasks
=============================

Grge implements two layers of issues using labels and task lists. The
aim is to have one issue board with stories and one with
tasks. Stories are used for planning, tasks for implementation.


Definitions
-----------

Story issue
  A story issue is an issue with the label *story*. It may have a section
  named *Tasks* that may contain
  `task lists <https://docs.gitlab.com/ee/user/markdown.html#task-lists>`_.
  A story may be *active* or *inactive*, indicated by the presence or
  absence of the label *active*.

Task
  A task is an task item in a top-level `task lists
  <https://docs.gitlab.com/ee/user/markdown.html#task-lists>`_ of
  story's *Tasks* section. Top-level list items that are not task
  items are ignored. If a corrsponding task issue has already been
  created, its iid is prefixing the task list item.

Task issue
  A task issue corresponds to a story's task list item. Its title
  is formed from the task list item, the story iid and the story
  title. A task may be *active* or *inactive*, indicated by the presence
  or absence of the label *active*.


Grge periodically pulls a project's story and task issues and
synchronizes each story's *tasks* with its *task issues*:

If a story is active, task issues are created for each of its
incomplete tasks, initially as well as when editing the task list of
an active story. After creating the task issues, the story's task list
is updated to reference these. In case grge gets interrupted, it will
recover independent of whether a story is active or not.

Activating a story, activates its tasks; deactivating a story,
deactivates its tasks.

For active stories a task is marked as complete if its task issue is
closed and as incomplete if it is re-opened.


Example config
--------------

grge-daemon.yaml::

  jobs:
    - sleep_interval: 30
      sync_stories_and_tasks:
        api_token: TOKEN
	gitlab_url: https://gitlab.com
	project_path: ApexAI/grge-test/combined

    - sleep_interval: 30
      # Interval to wait before polling and processing issues again

      sync_stories_and_tasks:
        api_token: TOKEN
	# API token with permissions to read and create issues

	gitlab_url: https://gitlab.com
	# URL to the gitlab instance

	project_path: ApexAI/grge-test/split
	# Path to project where stories are looked for

	task_project_path: ApexAI/grge-test/split-tasks
	# Optionally, place tasks into different project. Normally they live next to the story issues.

	active_story_label: sprint-story
	# Optionally, specify label that indicates an active story (default: 'active').

	active_task_label: sprint-task
	# Optionally, specify label that indicates an active task (default: 'active').

	story_labels: ['story', 'team']
	# List of labels that define a story, used to search for stories (default: ['story']).

	task_labels: ['task', 'team']
	# List of labels that define a task, used to search for and create tasks (default: ['task']).



Synchronization of external issues
==================================

Grge can synchronize issues from one or more source projects into a
target project. This is useful for example to integrate issues from
multiple projects into your internal project management.

For each source issue a target issue is created. The target issue
description contains a link to the source issue and some additional
information for it. This is stored in the so-called *preamble*,
everything up to the first heading. Everything from and including the
first heading is free to be used otherwise.

Example config
--------------

grge-daemon.yaml::

  jobs:
    - sleep_interval: 60
      sync_external_issues:
        sources:
          - gitlab_url: https://gitlab.com
            api_token: TOKEN
	    project_path: ApexAI/ade-cli

	    static_labels: [tooling, ade]
	    # Labels added to all target issues created for issues from this source

	    label_map:  # target to source label map
	      bug: bug
	      epic: epic
              story: story
              task: task

          - gitlab_url: https://gitlab.com
            api_token: TOKEN
            project_path: ApexAI/grge
            static_labels: [tooling, grge]
            label_map:
              bug: bug
              epic: epic
              story: story
              task: task

        target:
          gitlab_url: http://internal.gitlab
          api_token: TOKEN
          project_path: ApexAI/external-issues


Actions on issues and merge requests
====================================

Grge allows to perform actions on issues and merge requests::

  jobs:
    - sleep_interval: 60
      check_group_issues:
        api_token: TOKEN
	gitlab_url: URL
	group_path: GROUP
	rules:

          # Remove label foo and bar from closed issues
	  - filter: '(any ~foo ~bar) :state:closed'
	    actions:
	      - remove_labels: [foo, bar]

          # Ensure that issues have one of a set of labels
          - filter: '!grge:warning :state:opened'
	    checks:
	      - label_set: [a, b]
            actions:
              - add_labels: ['grge:warning']
              - reassign:
                  - '{item.author}'
              - comment:
                  mention:
                    - '{item.author}'
