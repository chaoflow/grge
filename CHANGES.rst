.. _changelog:

Changelog
---------

For the time being our versioning scheme is just one integer counting upwards.


.. _upcoming_changes:

Upcoming (unreleased)
^^^^^^^^^^^^^^^^^^^^^

Added
~~~~~

Changed
~~~~~~~

Deprecated
~~~~~~~~~~

Removed
~~~~~~~

Fixed
~~~~~

Security
~~~~~~~~


.. _v2:

2 (2019-06-18)
^^^^^^^^^^^^^^

Added
~~~~~
- Nix-shell setup for development
- Support synchronization of external issues
- Search capabilities for issues and merge requests, no cli yet
- Merge request and issue checking engine
- Create task issue with assignees and labels from story task list
- Synchronize task issue assignees to story task list
- Synchronize task issue labels to story task list

Changed
~~~~~~~
- Add and update Python dependencies and enable checking of hashes
- Update GitLab API code from marge-bot
- Extend GitLab API functionality (**breaking change to API**)
- New config format using dataclasses (**breaking change to config**)
- Switch to fully async architecture supporting multiple jobs
- Synchronize task issue title changes to story task list
- If stories and tasks are in different projects but in the same namespace they reference each other with project name only

Fixed
~~~~~
- Story and task sync does not update description if the description
  has been changed, still leaving room for a race condition


.. _v1:

1 (2019-05-02)
^^^^^^^^^^^^^^

Added
~~~~~
- Synchronization of task lists in Tasks section of story issues and dedicated task issues
- Support dry-run and running only once instead of looping
- Support tasks in same project as stories as well as dedicated project
- Enable configuration of the label indicating a story to be active; only active stories are synchronized
- Gitlab API and tests based on Smarkets `marge-bot <https://github.com/smarkets/marge-bot>`_ (see `./CREDITS`_).
- Robust handling of connection errors
