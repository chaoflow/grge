# -*- coding: utf-8 -*-
#
# Copyright 2019  Ternaris.
# SPDX-License-Identifier: Apache-2.0

from abc import ABCMeta, abstractmethod
from dataclasses import dataclass
from enum import Enum, auto
from typing import Optional, Tuple, Union

from .config import FString
from .utils import popwhile


class Regex(str):
    pass


@dataclass(frozen=True)
class Operator(metaclass=ABCMeta):
    pass


@dataclass(frozen=True)
class Greater(Operator):
    value: str


@dataclass(frozen=True)
class GreaterOrEqual(Operator):
    value: str


@dataclass(frozen=True)
class Smaller(Operator):
    value: str


@dataclass(frozen=True)
class SmallerOrEqual(Operator):
    value: str


Sourcepos = Tuple[int, int]


@dataclass(frozen=True)
class Criterion(metaclass=ABCMeta):  # type: ignore
    @abstractmethod
    def __call__(self, item) -> bool:
        """Run criterion on item."""


@dataclass(frozen=True)
class Author(Criterion):
    value: Union[str, FString, Regex]
    sourcepos: Optional[Sourcepos] = None

    def __call__(self, item) -> bool:
        return item.author.username == self.value


@dataclass(frozen=True)
class Assignee(Criterion):
    value: Union[str, FString, Regex]
    sourcepos: Optional[Sourcepos] = None

    def __call__(self, item) -> bool:
        return any(x.username == self.value for x in item.assignees)


@dataclass(frozen=True)
class Label(Criterion):
    value: Union[str, FString, Regex]
    sourcepos: Optional[Sourcepos] = None

    def __call__(self, item) -> bool:
        return self.value in item.labels


@dataclass(frozen=True)
class Milestone(Criterion):
    value: Union[str, FString, Regex]
    sourcepos: Optional[Sourcepos] = None

    def __call__(self, item) -> bool:
        if self.value.isnumeric():
            return int(self.value) == item.milestone.iid
        return self.value == item.milestone.title


@dataclass(frozen=True)
class State(Criterion):
    value: Union[str, FString, Regex]
    sourcepos: Optional[Sourcepos] = None

    def __call__(self, item) -> bool:
        return self.value == item.state


@dataclass(frozen=True)
class Negate(Criterion):
    criterion: Criterion
    sourcepos: Optional[Sourcepos] = None

    def __call__(self, item) -> bool:
        return not self.criterion(item)


CRITERIA_MAP = {
    'assignee': Assignee,
    'author': Author,
    'label': Label,
    'milestone': Milestone,
    'state': State,
}


@dataclass(frozen=True)
class Function(Criterion):  # type: ignore
    @abstractmethod
    def __call__(self, item) -> bool:
        """Run function on item."""


@dataclass(frozen=True)
class MatchAll(Function):
    criteria: Tuple[Criterion, ...]
    sourcepos: Optional[Sourcepos] = None

    def __call__(self, item) -> bool:
        return all(criterion(item) for criterion in self.criteria)

    # def __repr__(self):
    #     args = [repr(x) for x in self.criteria]
    #     args.append(f'sourcepos={repr(self.sourcepos)}')
    #     return f'MatchAll({", ".join(args)})'


@dataclass(frozen=True)
class MatchAny(Function):
    criteria: Tuple[Criterion, ...]
    sourcepos: Optional[Sourcepos] = None

    def __call__(self, item) -> bool:
        return any(criterion(item) for criterion in self.criteria)

    # def __repr__(self):
    #     args = [repr(x) for x in self.criteria]
    #     args.append(f'sourcepos={repr(self.sourcepos)}')
    #     return f'MatchAny({", ".join(args)})'


FUNCTION_MAP = {
    'all': MatchAll,
    'any': MatchAny,
}
FUNCTIONS = frozenset(FUNCTION_MAP.values())


class InvalidCharacter(Exception):
    pass


class _State(Enum):
    START = auto()
    FUNC_NAME = auto()
    FUNC_ARGS = auto()
    CRIT_NAME = auto()
    CRIT_VALUE = auto()


class _Substate(Enum):
    START = auto()
    ATTRIBUTE_PATH = auto()
    QUOTED = auto()
    REGEX = auto()
    STRING = auto()
    DONE = auto()


def parse_filter(string):
    # pylint: disable=too-many-statements,too-many-branches
    idx = 0
    stack = []
    token = []
    implicit_all = False
    state = _State.START
    substate = _Substate.START
    while idx < len(string):
        char = string[idx]
        # print(char, idx, state, substate)

        if state == _State.START:
            if char == ' ':
                pass

            elif char == '(':
                stack.append(idx)
                state = _State.FUNC_NAME

            else:
                implicit_all = True
                stack.extend((idx, MatchAll))
                state = _State.FUNC_ARGS
                idx -= 1

        elif state == _State.FUNC_NAME:
            if char == '_' or char.isalpha() and char.islower():
                token.append(char)

            elif char in ' )':
                stack.append(FUNCTION_MAP[_make_token(token)])
                state = _State.FUNC_ARGS
                if char == ')':
                    _make_function(stack, idx)

            else:
                raise InvalidCharacter(string[idx], idx + 1, string)

        elif state == _State.CRIT_NAME:
            if char == '_' or char.isalpha() and char.islower():
                token.append(char)

            elif char == ':':
                stack.append(CRITERIA_MAP[_make_token(token)])
                state = _State.CRIT_VALUE

            else:
                raise InvalidCharacter(string[idx], idx + 1, string)

        elif state == _State.FUNC_ARGS:
            if char == ' ':
                pass

            elif char == '(':
                stack.append(idx)
                state = _State.FUNC_NAME

            elif char == ')':
                _make_function(stack, idx)

            elif char == '!':
                stack.extend((idx, Negate))

            elif char == '~':
                stack.extend((idx, Label))
                state = _State.CRIT_VALUE

            elif char == '%':
                stack.extend((idx, Milestone))
                state = _State.CRIT_VALUE

            elif char == ':':
                stack.append(idx)
                state = _State.CRIT_NAME

            else:
                raise InvalidCharacter(string[idx], idx + 1, string)

        elif state == _State.CRIT_VALUE:
            if substate == _Substate.START:
                if char == '"':
                    substate = _Substate.QUOTED

                elif char == '{':
                    substate = _Substate.ATTRIBUTE_PATH

                elif char == '/':
                    substate = _Substate.REGEX

                elif char in '<>':
                    if string[idx:idx+2] == '>=':
                        stack.append(GreaterOrEqual)
                        idx += 1
                    elif string[idx:idx+2] == '<=':
                        stack.append(SmallerOrEqual)
                        idx += 1
                    elif char == '>':
                        stack.append(Greater)
                    elif char == '<':  # pragma: no branch
                        stack.append(Smaller)

                elif char.isalpha() or char.isdigit():
                    substate = _Substate.STRING
                    idx -= 1

                else:
                    raise InvalidCharacter(string[idx], idx + 1, string)

            elif substate == _Substate.ATTRIBUTE_PATH:
                if char == '}':
                    stack.append(FString(f'{_make_token(token)}'))
                    substate = _Substate.DONE
                else:
                    token.append(char)

            elif substate == _Substate.QUOTED:
                if char == '"':
                    stack.append(_make_token(token))
                    substate = _Substate.DONE
                else:
                    token.append(char)

            elif substate == _Substate.REGEX:
                if char == '/':
                    stack.append(Regex(_make_token(token)))
                    substate = _Substate.DONE
                else:
                    token.append(char)

            elif substate == _Substate.STRING:
                if char in ' )':
                    stack.append(_make_token(token))
                    substate = _Substate.DONE

                elif idx + 1 == len(string):
                    token.append(char)
                    stack.append(_make_token(token))
                    substate = _Substate.DONE

                else:
                    token.append(char)

            else:
                raise RuntimeError(f'Unknown subtate {substate}.')  # pragma: no cover

            if substate == _Substate.DONE:
                _make_criterion(stack, idx - 1 if char in ' )' else idx)
                if char == ')':
                    _make_function(stack, idx)
                state = _State.FUNC_ARGS
                substate = _Substate.START

        else:
            raise RuntimeError(f'Unknown state {state}.')  # pragma: no cover

        idx += 1
        # print('-->', token, stack)

    if implicit_all:
        if len(stack) == 3:
            return stack[2]
        _make_function(stack, idx - 1)

    if not stack:
        return MatchAll(())

    assert len(stack) == 1, stack
    assert isinstance(stack[0], Function)
    return stack[0]


def _make_token(token):
    result = ''.join(token)
    token[:] = []
    return result


def _make_function(stack, stop_idx):
    criteria = popwhile(lambda x: x not in FUNCTIONS, stack)
    func_class = stack.pop()
    idx = stack.pop()
    assert isinstance(idx, int)
    func = func_class(tuple(criteria), sourcepos=(idx + 1, stop_idx + 1))
    _check_negate_and_append(stack, func, stop_idx)


def _make_criterion(stack, stop_idx):
    arg, crit_class = stack.pop(), stack.pop()
    if issubclass(crit_class, Operator):
        arg = crit_class(arg)
        crit_class = stack.pop()
    idx = stack.pop()
    assert issubclass(crit_class, Criterion), crit_class
    assert isinstance(idx, int), idx
    crit = crit_class(arg, sourcepos=(idx + 1, stop_idx + 1))
    _check_negate_and_append(stack, crit, stop_idx)


def _check_negate_and_append(stack, crit, stop_idx):
    if stack and stack[-1] is Negate:
        _, idx = stack.pop(), stack.pop()
        stack.append(Negate(crit, sourcepos=(idx + 1, stop_idx + 1)))
    else:
        stack.append(crit)
