# -*- coding: utf-8 -*-
#
# Copyright 2018 - 2019  Ternaris.
# SPDX-License-Identifier: Apache-2.0

import logging

from . import gitlab
from .project import Project
from .story import StoryIssue
from .task import TaskIssue

LOG = logging.getLogger(__name__)


class StoryTaskSyncer:
    _story_project = None
    _task_project = None

    def __init__(self, cfg):
        self._cfg = cfg
        self._api = gitlab.get_api(cfg.gitlab_url, cfg.api_token)

    @property
    def api(self):
        return self._api

    @property
    def cfg(self):
        return self._cfg

    async def story_project(self):
        if self._story_project is None:
            self._story_project = await Project.fetch_by_path(self.cfg.project_path, self.api)
        return self._story_project

    async def task_project(self):
        if self._task_project is None:
            if self.cfg.task_project_path is None:
                project = await self.story_project()
            else:
                project = await Project.fetch_by_path(self.cfg.task_project_path, self.api)
            self._task_project = project
        return self._task_project

    async def fetch_story_issues(self):
        story_project = await self.story_project()
        task_project = await self.task_project()
        issues = await StoryIssue.search_project(
            story_project.api,
            story_project.id,
            {
                'labels': ','.join(sorted(self.cfg.story_labels)),
                'state': 'opened',
            },
        )
        LOG.info('Fetched %s story issues from %s', len(issues), story_project.path_with_namespace)
        for story in issues:
            story.active_story_label = self.cfg.active_story_label
            story.active_task_label = self.cfg.active_task_label
            story.story_project = story_project
            story.task_project = task_project
            story.task_labels = tuple(self.cfg.task_labels)
        return issues

    async def fetch_task_issues(self, story_issues):
        task_project = await self.task_project()
        open_issues = await TaskIssue.search_project(
            task_project.api,
            task_project.id,
            {
                'labels': ','.join(sorted(self.cfg.task_labels)),
                'state': 'opened',
            },
        )
        wanted_iids = {
            task.iid
            for story in story_issues
            for task in story.tasks
            if task.iid
        }
        missing_iids = wanted_iids - {issue.iid for issue in open_issues}
        issues = open_issues + await TaskIssue.search_project(
            task_project.api,
            task_project.id,
            {
                'labels': ','.join(sorted(self.cfg.task_labels)),
                'state': 'closed',
                'iids[]': tuple(missing_iids),
            },
        )
        LOG.info('Fetched %s task issues from %s', len(issues), task_project.path_with_namespace)
        return issues

    async def run(self):
        story_issues = await self.fetch_story_issues()
        task_issues = {x.iid: x for x in await self.fetch_task_issues(story_issues)}
        for story in story_issues:
            await story.synchronize_tasks(task_issues)
