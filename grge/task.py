# -*- coding: utf-8 -*-
#
# Copyright 2018 - 2019  Ternaris.
# SPDX-License-Identifier: Apache-2.0

import re
from dataclasses import dataclass, field, replace
from typing import List, Optional, Tuple

import commonmark

from .issue import Issue
from .markdown import InvalidCharacter, iterate_children, parse_task_trailer


class TaskIssue(Issue):
    pass


@dataclass(frozen=True)
class Task:
    title: str
    done: Optional[bool] = False
    iid: Optional[int] = None
    sourcepos: Optional[List[List[int]]] = None
    story: Optional[Issue] = None
    project_path: Optional[str] = None
    assignees: Tuple[str, ...] = field(default_factory=tuple)
    labels: Tuple[str, ...] = field(default_factory=tuple)

    @classmethod
    def extract_all(cls, document):
        return [x for x in extract_tasks(document)]

    @classmethod
    def parse(cls, string, sourcepos=None):
        match = re.match(
            (r'^'
             r'\[(?P<done>[x ])\]\s'
             r'(?:(?P<project_path>\S+)?#(?P<iid>\d+)\s)?'
             r'\s*(?P<title>[^@~]+)'
             r'(?P<trailer>.*)$'),
            string,
        )
        if not match:
            raise ValueError(string)

        dct = match.groupdict()
        dct['title'] = dct['title'].strip()
        dct['done'] = dct['done'] == 'x'
        dct['iid'] = int(dct['iid']) if dct['iid'] else None
        trailer = dct.pop('trailer', '')
        if trailer:
            try:
                dct.update(parse_task_trailer(trailer))
            except InvalidCharacter:
                pass
        return cls(sourcepos=sourcepos, **dct)

    def replace(self, **kw):
        return replace(self, **kw)

    def render(self):
        parts = []
        parts.append('[x]' if self.done else '[ ]')
        if self.iid:
            parts.append(f'{self.project_path or ""}#{self.iid}')
        parts.append(self.title)
        parts.extend(f'@{x}' for x in sorted(self.assignees))
        parts.extend(f'~{self._quote(x)}' for x in sorted(self.labels))
        return ' '.join(x for x in parts if x)

    @staticmethod
    def _quote(label):
        if label.isalnum():
            return label
        label = label.replace('"', '\\"')
        return f'"{label}"'


def extract_tasks(document):
    """Iterate over tasks in Tasks section.

    Within the Tasks section a task is a top-level list element with a
    checkbox.
    """
    parser = commonmark.Parser()
    ast = parser.parse(document)
    lines = document.splitlines()
    in_tasks_section = False
    for node in iterate_children(ast):
        if node.t == 'heading':
            if in_tasks_section:
                return
            if node.first_child.t == 'text' and node.first_child.literal == 'Tasks':
                in_tasks_section = True

        elif not in_tasks_section:
            continue

        elif node.t == 'list':
            for task in _extract_tasks_from_list(node, lines):
                yield task


def _extract_tasks_from_list(node, lines):
    for item in iterate_children(node):
        # Probably unnecessary safety measure
        if item.t != 'item' or item.first_child.t != 'paragraph':
            continue  # pragma: no cover

        sourcepos = item.first_child.sourcepos
        if sourcepos[0][0] != sourcepos[1][0]:
            continue

        row_idx = sourcepos[0][0] - 1
        col_idx = sourcepos[0][1] - 1
        string = lines[row_idx][col_idx:]
        try:
            yield Task.parse(string, sourcepos)
        except ValueError:
            pass
