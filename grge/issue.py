# -*- coding: utf-8 -*-
#
# Copyright 2018 - 2019  Ternaris.
# SPDX-License-Identifier: Apache-2.0

import logging as log
from collections import namedtuple
from typing import Any, Dict, List, Iterable, Tuple, Union
from urllib.parse import quote_plus

import pendulum

from . import gitlab
from .user import User


GET, POST, PUT = gitlab.GET, gitlab.POST, gitlab.PUT


# pylint: disable=too-many-public-methods
class Issue(gitlab.Resource):

    @classmethod
    async def create(cls, api: gitlab.Api, id_or_path: Union[int, str], params: Dict) -> 'Issue':
        labels = params.pop('labels', None)
        if labels is not None:
            assert isinstance(labels, (tuple, list, set)), labels
            labels = set(labels)
            bad_labels = {x for x in labels if ',' in x}
            assert not bad_labels, f'Labels must not contain comma: {bad_labels!r}'
            params['labels'] = ','.join(sorted(labels))

        log.info('Create %s: %s, %r', cls.__name__, id_or_path, params)
        id_or_path = quote_plus(id_or_path) if isinstance(id_or_path, str) else id_or_path
        info = await api.call(POST(f'/projects/{id_or_path}/issues', params))
        return cls(api, info)

    @classmethod
    async def fetch_by_iid(cls, api: gitlab.Api, id_or_path: Union[int, str], iid: int) -> 'Issue':
        id_or_path = quote_plus(id_or_path) if isinstance(id_or_path, str) else id_or_path
        issue = cls(api, {'iid': iid, 'project_id': id_or_path})
        await issue.refetch_info()
        return issue

    @classmethod
    async def search_group(
            cls,
            api: gitlab.Api,
            id_or_path: Union[int, str],
            params: Dict = None
    ) -> List['Issue']:
        id_or_path = quote_plus(id_or_path) if isinstance(id_or_path, str) else id_or_path
        infos = await api.collect_all_pages(GET(f'/groups/{id_or_path}/issues', params))
        return [cls(api, info) for info in infos]

    @classmethod
    async def search_project(
            cls,
            api: gitlab.Api,
            id_or_path: Union[int, str],
            params: Dict = None
    ) -> List['Issue']:
        id_or_path = quote_plus(id_or_path) if isinstance(id_or_path, str) else id_or_path
        infos = await api.collect_all_pages(GET(f'/projects/{id_or_path}/issues', params))
        return [cls(api, info) for info in infos]

    def __repr__(self) -> str:
        return f'<{self.__class__.__name__} #{self.iid}>'

    @property
    def author(self) -> User:
        return User(self.api, self.info['author'])

    @property
    def assignees(self) -> List[User]:
        return [User(self.api, x) for x in self.info['assignees']]

    @property
    def iid(self) -> int:
        return self.info['iid']

    @property
    def description(self) -> str:
        return self.info['description'] or ''

    @property
    def labels(self) -> Tuple[str, ...]:
        return tuple(self.info['labels'])

    @property
    def milestone(self):  # TODO
        # TODO: Replace with proper milestone representation
        info = self.info['milestone']
        return namedtuple('Attrs', info.keys())(*info.values())

    @property
    def project_id(self) -> int:
        return self.info['project_id']

    @property
    def state(self) -> str:
        return self.info['state']

    @property
    def title(self) -> str:
        return self.info['title']

    @property
    def web_url(self) -> str:
        return self.info['web_url']

    @property
    def created_at(self) -> pendulum.DateTime:
        return pendulum.parse(self.info['created_at'])

    @property
    def closed_at(self) -> pendulum.DateTime:
        return pendulum.parse(self.info['closed_at'])

    @property
    def updated_at(self) -> pendulum.DateTime:
        return pendulum.parse(self.info['updated_at'])

    def __eq__(self, other: Any) -> bool:
        return self.info == other.info

    async def comment(self, message: str):
        if (await self._api.version()).release >= (9, 2, 2):
            notes_url = f'/projects/{self.project_id}/issues/{self.iid}/notes'
        else:
            # GitLab botched the v4 api before 9.2.2
            notes_url = f'/projects/{self.project_id}/issues/{self.id}/notes'

        return await self._api.call(POST(notes_url, {'body': message}))

    async def update_labels(self, add: Iterable[str], remove: Iterable[str]) -> None:
        current = set(self.labels)
        wanted = current | set(add) - set(remove)
        if current == wanted:
            return

        await self.refetch_info()
        current = set(self.labels)
        wanted = current | set(add) - set(remove)
        if current == wanted:
            return

        bad_labels = {x for x in wanted if ',' in x}
        assert not bad_labels, f'Labels must not contain comma: {bad_labels!r}'
        params = {'labels': ','.join(sorted(wanted))}
        log.info('Update %s %s#%s: %r', type(self).__name__, self.project_id, self.iid, params)
        info = await self._api.call(PUT(f'/projects/{self.project_id}/issues/{self.iid}', params))
        self._info = info

    async def put_and_update(self, params: Dict) -> None:
        labels = params.pop('labels', None)
        if labels is not None:
            assert isinstance(labels, (tuple, list, set)), labels
            labels = set(labels)
            add = labels.difference(self.labels)
            remove = set(self.labels) - labels
            await self.update_labels(add, remove)

        if not params:
            return

        log.info('Update %s %s#%s: %r', type(self).__name__, self.project_id, self.iid, params)
        info = await self._api.call(PUT(f'/projects/{self.project_id}/issues/{self.iid}', params))
        self._info = info

    async def refetch_info(self) -> None:
        self._info = await self._api.call(GET(f'/projects/{self.project_id}/issues/{self.iid}'))
