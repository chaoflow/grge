# -*- coding: utf-8 -*-
#
# Copyright 2019  Ternaris.
# SPDX-License-Identifier: Apache-2.0

import logging
import re
from typing import NamedTuple, Set, Tuple
from urllib.parse import urlparse

from . import gitlab
from .config import SyncExternalIssues
from .issue import Issue
from .markdown import extract_preamble_and_rest

LOG = logging.getLogger(__name__)


class BrokenTargetIssue(Exception):
    """A broken target issue has been encountered."""


class Info(NamedTuple):
    issue: Issue
    preamble: str
    managed_labels: Set[str]
    wanted_labels: Tuple[str, ...]


class ExternalIssuesSyncer:
    def __init__(self, cfg: SyncExternalIssues):
        self._cfg = cfg

    @property
    def cfg(self):
        return self._cfg

    @staticmethod
    def _get_labels(source_cfg, issue):
        return tuple(
            target_label for target_label, source_label in source_cfg.label_map.items()
            if source_label in issue.labels
        ) + tuple(source_cfg.static_labels)

    @staticmethod
    def _render_user(user):
        return f'[{user.username}]({user.web_url})'

    def _render_preamble(self, source_issue):
        return (
            f'- upstream: {source_issue.web_url}\n'
            f'  - author: {self._render_user(source_issue.author)}\n'
            f'  - assignees: {", ".join(self._render_user(x) for x in source_issue.assignees)}\n'
            f'  - labels: {", ".join(sorted(source_issue.labels))}\n'
        )

    @staticmethod
    def _source_key(source, issue):
        return (source.gitlab_url, source.project_path, issue.iid)

    @staticmethod
    def _target_key(issue):
        preamble = extract_preamble_and_rest(issue.description)[0]
        try:
            upstream = re.search(r'- upstream: (\S+)', preamble).groups()[0]
        except AttributeError:
            raise BrokenTargetIssue(issue.web_url)
        parts = urlparse(upstream)
        gitlab_url = '://'.join([parts.scheme, parts.netloc])
        project, _, iid = parts.path[1:].rsplit('/', 2)
        return (gitlab_url, project, int(iid))

    @staticmethod
    async def fetch_target_issues(target):
        api = gitlab.get_api(target.gitlab_url, target.api_token)
        issues = await Issue.search_project(api, target.project_path, {
            'labels': target.managed_label,
        })
        LOG.info('Fetched %s issues from %s', len(issues), target.project_path)
        return issues

    @staticmethod
    async def fetch_source_issues(source):
        api = gitlab.get_api(source.gitlab_url, source.api_token)
        issues = await Issue.search_project(api, source.project_path, {
            'state': 'opened',
        })
        LOG.info('Fetched %s issues from %s', len(issues), source.project_path)
        return issues

    async def run(self):
        # Fetch all target issues we are managing
        target_issues = {
            self._target_key(x): x for x in await self.fetch_target_issues(self.cfg.target)
        }

        # Fetch all open issues for source projects
        source_issues = {}
        for source in self.cfg.sources:
            source_issues.update(
                (self._source_key(source, x), Info(
                    issue=x,
                    preamble=self._render_preamble(x),
                    managed_labels=set(source.label_map.keys()),
                    wanted_labels=self._get_labels(source, x),
                ))
                for x in await self.fetch_source_issues(source)
            )

        # Synchronize existing and create missing target issues
        target_api = gitlab.get_api(self.cfg.target.gitlab_url, self.cfg.target.api_token)
        for key, info in source_issues.items():
            target_issue = target_issues.pop(key, None)

            if target_issue is None:
                wanted_labels = (self.cfg.target.managed_label,) + info.wanted_labels
                await Issue.create(target_api, self.cfg.target.project_path, {
                    'title': info.issue.title,
                    'description': info.preamble,
                    'labels': wanted_labels,
                })
                continue

            update = {}
            current_labels = set(target_issue.labels)
            wanted_labels = (current_labels - info.managed_labels).union(info.wanted_labels)
            if current_labels != wanted_labels:
                update['labels'] = wanted_labels

            if target_issue.state == 'closed':
                update['state_event'] = 'reopen'

            preamble, rest = extract_preamble_and_rest(target_issue.description)
            if preamble.strip() != info.preamble.strip():
                update['description'] = '\n'.join([info.preamble, rest])

            if update:
                await target_issue.put_and_update(update)

        # Close all target issues for which no open source issues exist
        for issue in target_issues.values():
            if issue.state == 'opened':
                await issue.put_and_update({'state_event': 'close'})
