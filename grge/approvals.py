# -*- coding: utf-8 -*-
#
# Copyright 2019  Ternaris.
# SPDX-License-Identifier: Apache-2.0
#
# This module contains code originally published
# Copyright 2017 Smarkets Limited
# SPDX-License-Identifier: BSD-3-Clause

import asyncio
from typing import List
from . import gitlab

GET, POST = gitlab.GET, gitlab.POST


class Approvals(gitlab.Resource):
    """Approval info for a MergeRequest."""

    async def refetch_info(self) -> None:
        gitlab_version = await self._api.version()
        if gitlab_version.release >= (9, 2, 2):
            approver_url = '/projects/{0.project_id}/merge_requests/{0.iid}/approvals'.format(self)
        else:
            # GitLab botched the v4 api before 9.2.3
            approver_url = '/projects/{0.project_id}/merge_requests/{0.id}/approvals'.format(self)

        if gitlab_version.is_ee:
            self._info = await self._api.call(GET(approver_url))
        else:
            self._info = dict(self._info, approvals_left=0, approved_by=[])

    @property
    def iid(self) -> int:
        return self.info['iid']

    @property
    def project_id(self) -> int:
        return self.info['project_id']

    @property
    def approvals_left(self) -> int:
        return self.info['approvals_left'] or 0

    @property
    def sufficient(self) -> bool:
        return not self.info['approvals_left']

    @property
    def approver_usernames(self) -> List[str]:
        return [who['user']['username'] for who in self.info['approved_by']]

    @property
    def approver_ids(self) -> List[int]:
        """Return the uids of the approvers."""
        return [who['user']['id'] for who in self.info['approved_by']]

    async def reapprove(self) -> None:
        """Impersonates the approvers and re-approves the merge_request as them.

        The idea is that we want to get the approvers, push the rebased branch
        (which may invalidate approvals, depending on GitLab settings) and then
        restore the approval status.
        """
        if (await self._api.version()).release >= (9, 2, 2):
            approve_url = '/projects/{0.project_id}/merge_requests/{0.iid}/approve'.format(self)
        else:
            # GitLab botched the v4 api before 9.2.3
            approve_url = '/projects/{0.project_id}/merge_requests/{0.id}/approve'.format(self)

        tasks = [self._api.call(POST(approve_url), sudo=uid) for uid in self.approver_ids]
        asyncio.gather(*tasks)
