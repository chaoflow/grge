# -*- coding: utf-8 -*-
#
# Copyright 2018 - 2019  Ternaris.
# SPDX-License-Identifier: Apache-2.0

import asyncio
import logging as log
import os
import re
import signal
from dataclasses import dataclass
from itertools import count
from typing import Awaitable, Dict, List, Type

import aiohttp.client_exceptions
from . import config
from . import gitlab
from .check_issues_and_mrs import GroupIssueChecker, GroupMergeRequestChecker
from .check_issues_and_mrs import ProjectIssueChecker, ProjectMergeRequestChecker
from .sync_external_issues import ExternalIssuesSyncer
from .sync_story_task import StoryTaskSyncer
from .issue import Issue

PDB = os.environ.get('PDB')


JOBS_MAP = {
    config.SyncExternalIssuesJob: ExternalIssuesSyncer,
    config.SyncStoriesAndTasksJob: StoryTaskSyncer,
    config.CheckGroupIssuesJob: GroupIssueChecker,
    config.CheckProjectIssuesJob: ProjectIssueChecker,
    config.CheckGroupMergeRequestsJob: GroupMergeRequestChecker,
    config.CheckProjectMergeRequestsJob: ProjectMergeRequestChecker,
}


@dataclass(frozen=True)
class JobWrapper:
    cfg: config.JobUnion
    idx: int

    @property
    def jobcfg(self):
        attrname = re.sub(r'([a-z0-9])([A-Z])', r'\1_\2', type(self.cfg).__name__[:-3]).lower()
        return getattr(self.cfg, attrname)

    @property
    def jobcls(self):
        return JOBS_MAP[type(self.cfg)]

    @property
    def name(self) -> str:
        return self.cfg.name or f'{self.idx}'

    async def run(self, once: bool = False) -> None:
        job = self.jobcls(self.jobcfg)
        if once:
            await job.run()
            return

        while True:
            try:
                await job.run()
            except (aiohttp.client_exceptions.ClientError,
                    asyncio.TimeoutError,
                    gitlab.ApiError) as exc:
                log.warning('%r', exc)
                if PDB:
                    raise
            except Exception:  # pylint: disable=broad-except
                log.exception('Exception raised running job %s', self.name)
                if PDB:
                    raise
            await asyncio.sleep(self.cfg.sleep_interval)


class Daemon:
    def __init__(self, cfg: Awaitable[config.Config]):
        self._cfg = cfg
        self._tasks = []  # type: List[asyncio.Task]

    async def run(self, once: bool = False, dry_run: bool = False) -> None:
        """Run daemon.

        Args:
            once (bool): Perform each job once, then exit
            dry_run (bool): Perform a dry run
        """
        cfg = await self._cfg
        if dry_run:
            self._prepare_dry_run()
        self._tasks = [
            asyncio.create_task(JobWrapper(job, idx).run(once=once))
            for idx, job in enumerate(cfg.jobs)
        ]
        await asyncio.gather(*self._tasks)
        await asyncio.gather(*[(await api.session()).close() for api in gitlab.APIS.values()])

    async def abort(self) -> None:
        loop = asyncio.get_event_loop()
        loop.remove_signal_handler(signal.SIGINT)
        loop.remove_signal_handler(signal.SIGTERM)
        await asyncio.gather(*[(await api.session()).close() for api in gitlab.APIS.values()])
        while self._tasks:
            task = self._tasks.pop(0)
            task.cancel()

    def install_signal_handlers(self) -> None:
        loop = asyncio.get_event_loop()
        loop.add_signal_handler(signal.SIGINT, lambda: asyncio.create_task(self.abort()))
        loop.add_signal_handler(signal.SIGTERM, lambda: asyncio.create_task(self.abort()))

    @staticmethod
    def _prepare_dry_run() -> None:
        _real_api_call = gitlab.Api.call

        async def _api_call(self, command, sudo=None):
            assert isinstance(command, gitlab.GET), command
            return await _real_api_call(self, command, sudo=sudo)
        gitlab.Api.call = _api_call  # type: ignore

        iid_counter = count(420001)

        @classmethod  # type: ignore
        async def _issue_create(
                cls: Type[Issue], api: gitlab.Api, project_id: int, params: Dict
        ) -> Issue:  # pylint: disable=unused-argument
            log.info('DRYRUN: Create %s: %s, %r', cls.__name__, project_id, params)
            info = params.copy()
            info['iid'] = next(iid_counter)
            info['state'] = 'opened'
            info['labels'] = [x for x in info.get('labels', '').split(',') if x]
            return cls(api, info)  # pylint: disable=too-many-function-args

        async def _put_and_update(self, params: Dict) -> None:
            log.info('DRYRUN: Update %s %s#%s: %r',
                     type(self).__name__, self.project_id, self.iid, params)

        Issue.create = _issue_create  # type: ignore
        Issue.put_and_update = _put_and_update  # type: ignore
