# -*- coding: utf-8 -*-
#
# Copyright 2019  Ternaris.
# SPDX-License-Identifier: Apache-2.0
#
# This module contains code originally published
# Copyright 2017 Smarkets Limited
# SPDX-License-Identifier: BSD-3-Clause

import logging as log
import time
from typing import Dict, List, Optional, Tuple, Union
from urllib.parse import quote_plus

from . import gitlab
from .user import User
from .approvals import Approvals


GET, POST, PUT, DELETE = gitlab.GET, gitlab.POST, gitlab.PUT, gitlab.DELETE


# pylint: disable=too-many-public-methods
class MergeRequest(gitlab.Resource):

    @classmethod
    async def create(cls, api: gitlab.Api, project_id: int, params: Dict) -> 'MergeRequest':
        merge_request_info = await api.call(POST(
            '/projects/{project_id}/merge_requests'.format(project_id=project_id),
            params,
        ))
        merge_request = cls(api, merge_request_info)
        return merge_request

    @classmethod
    async def fetch_by_iid(
            cls, project_id: int, merge_request_iid: int, api: gitlab.Api
    ) -> 'MergeRequest':
        merge_request = cls(api, {'iid': merge_request_iid, 'project_id': project_id})
        await merge_request.refetch_info()
        return merge_request

    @classmethod
    async def search_group(
            cls, api: gitlab.Api, id_or_path: Union[int, str], params: Dict = None
    ) -> List['MergeRequest']:
        id_or_path = quote_plus(id_or_path) if isinstance(id_or_path, str) else id_or_path
        infos = await api.collect_all_pages(GET(f'/groups/{id_or_path}/merge_requests', params))
        return [cls(api, info) for info in infos]

    @classmethod
    async def search_project(
            cls, api: gitlab.Api, id_or_path: Union[int, str], params: Dict = None
    ) -> List['MergeRequest']:
        id_or_path = quote_plus(id_or_path) if isinstance(id_or_path, str) else id_or_path
        infos = await api.collect_all_pages(GET(f'/projects/{id_or_path}/merge_requests', params))
        return [cls(api, info) for info in infos]

    @classmethod
    async def fetch_all_open_for_user(
            cls, project_id: int, user_id: int, api: gitlab.Api, merge_order: str
    ) -> List['MergeRequest']:
        all_merge_request_infos = await api.collect_all_pages(GET(
            '/projects/{project_id}/merge_requests'.format(project_id=project_id),
            {'state': 'opened', 'order_by': merge_order, 'sort': 'asc'},
        ))
        my_merge_request_infos = [
            mri for mri in all_merge_request_infos
            if (mri['assignee'] or {}).get('id') == user_id
        ]

        return [cls(api, merge_request_info) for merge_request_info in my_merge_request_infos]

    @property
    def project_id(self) -> int:
        return self.info['project_id']

    @property
    def iid(self) -> int:
        return self.info['iid']

    @property
    def title(self) -> str:
        return self.info['title']

    @property
    def labels(self) -> Tuple[str, ...]:
        return tuple(self.info['labels'])

    @property
    def author(self) -> User:
        return User(self.api, self.info['author'])

    @property
    def assignee(self) -> Optional[User]:
        return User(self.api, self.info['assignee']) if self.info['assignee'] else None

    @property
    def assignees(self) -> List[User]:
        return [User(self.api, x) for x in self.info['assignees']]

    @property
    def state(self) -> str:
        return self.info['state']

    @property
    def rebase_in_progress(self) -> bool:
        return self.info.get('rebase_in_progress', False)

    @property
    def merge_error(self):
        return self.info.get('merge_error')

    @property
    def assignee_id(self):
        assignee = self.info['assignee'] or {}
        return assignee.get('id')

    @property
    def author_id(self):
        return self.info['author'].get('id')

    @property
    def source_branch(self):
        return self.info['source_branch']

    @property
    def target_branch(self):
        return self.info['target_branch']

    @property
    def sha(self):
        return self.info['sha']

    @property
    def squash(self):
        return self.info.get('squash', False)  # missing means auto-squash not supported

    @property
    def source_project_id(self):
        return self.info['source_project_id']

    @property
    def target_project_id(self):
        return self.info['target_project_id']

    @property
    def work_in_progress(self):
        return self.info['work_in_progress']

    @property
    def approved_by(self):
        return self.info['approved_by']

    @property
    def web_url(self):
        return self.info['web_url']

    async def refetch_info(self):
        cmd = GET(f'/projects/{self.project_id}/merge_requests/{self.iid}')
        self._info = await self._api.call(cmd)

    async def put_and_update(self, params):
        labels = params.get('labels')
        if labels and isinstance(labels, (tuple, list)):
            params['labels'] = ','.join(sorted(labels))

        log.info('Update %s %s!%s: %r', type(self).__name__, self.project_id, self.iid, params)
        info = await self._api.call(
            PUT(f'/projects/{self.project_id}/merge_requests/{self.iid}', params)
        )
        self._info = info

    async def comment(self, message):
        if (await self._api.version()).release >= (9, 2, 2):
            notes_url = f'/projects/{self.project_id}/merge_requests/{self.iid}/notes'
        else:
            # GitLab botched the v4 api before 9.2.2
            notes_url = f'/projects/{self.project_id}/merge_requests/{self.id}/notes'

        return await self._api.call(POST(notes_url, {'body': message}))

    async def rebase(self):
        await self.refetch_info()

        if not self.rebase_in_progress:
            await self._api.call(PUT(
                '/projects/{0.project_id}/merge_requests/{0.iid}/rebase'.format(self),
            ))
        else:
            # We wanted to rebase and someone just happened to press the button for us!
            log.info('A rebase was already in progress on the merge request!')

        max_attempts = 30
        wait_between_attempts_in_secs = 1

        for _ in range(max_attempts):
            self.refetch_info()
            if not self.rebase_in_progress:
                if self.merge_error:
                    raise MergeRequestRebaseFailed(self.merge_error)
                return

            time.sleep(wait_between_attempts_in_secs)

        raise TimeoutError('Waiting for merge request to be rebased by GitLab')

    async def accept(self, remove_branch=False, sha=None):
        return await self._api.call(PUT(
            '/projects/{0.project_id}/merge_requests/{0.iid}/merge'.format(self),
            dict(
                remove_source_branch=remove_branch,
                merge_when_pipeline_succeeds=True,
                # if provided, ensures what is merged is what we want (or fails)
                sha=sha or self.sha,
            ),
        ))

    async def close(self):
        return await self._api.call(PUT(
            '/projects/{0.project_id}/merge_requests/{0.iid}'.format(self),
            {'state_event': 'close'},
        ))

    async def assign_to(self, user_id):
        return await self._api.call(PUT(
            '/projects/{0.project_id}/merge_requests/{0.iid}'.format(self),
            {'assignee_id': user_id},
        ))

    def unassign(self):
        return self.assign_to(None)

    def fetch_approvals(self):
        # 'id' needed for for GitLab 9.2.2 hack (see Approvals.refetch_info())
        info = {'id': self.id, 'iid': self.iid, 'project_id': self.project_id}
        approvals = Approvals(self.api, info)
        approvals.refetch_info()
        return approvals

    async def fetch_commits(self):
        cmd = GET(f'/projects/{self.project_id}/merge_requests/{self.iid}/commits')
        return await self._api.call(cmd)


class MergeRequestRebaseFailed(Exception):
    pass
