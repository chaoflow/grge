# -*- coding: utf-8 -*-
#
# Copyright 2019  Ternaris.
# SPDX-License-Identifier: Apache-2.0

import logging
from abc import ABCMeta, abstractmethod
from typing import AsyncGenerator
from typing import Dict, FrozenSet, Iterable, List, Optional, Set, Tuple, Type, Union

from . import config
from . import gitlab
from . import search
from .issue import Issue
from .markdown import join_labels
from .merge_request import MergeRequest
from .search import parse_filter
from .user import User

LOG = logging.getLogger(__name__)
Item = Union[Issue, MergeRequest]


class LabelSetCheck:

    def __init__(self, cfg: config.LabelSetCheck) -> None:
        self._cfg = cfg
        self._label_set = frozenset(cfg.label_set)  # type: FrozenSet[str]

    @property
    def cfg(self) -> config.LabelSetCheck:
        return self._cfg

    @property
    def label_set(self) -> FrozenSet[str]:
        return self._label_set

    @property
    def required(self) -> bool:
        return self.cfg.required

    def run(self, item: Item) -> Optional[str]:
        intersection = self.label_set.intersection(item.labels)
        if len(intersection) > 1:
            return f'Only one of {join_labels(self.label_set)} allowed!'

        if self.required and not intersection:
            return f'One of {join_labels(self.label_set)} is required!'

        return None


CHECK_MAP = {
    config.LabelSetCheck: LabelSetCheck,
}


class Action(metaclass=ABCMeta):
    def __init__(self, cfg: config.ActionUnion, msgs: Iterable[str] = None) -> None:
        self._cfg = cfg
        self._msgs = tuple(msgs if msgs else [])

    @property
    def cfg(self):
        return self._cfg

    @property
    def msgs(self) -> Tuple[str, ...]:
        return self._msgs

    @abstractmethod
    async def __call__(self, item: Item) -> None:
        pass  # pragma: no cover


class AddLabelsAction(Action):
    def __init__(self, cfg: config.AddLabelsAction, msgs: Iterable[str] = None) -> None:
        super().__init__(cfg, msgs)
        self._cfg = cfg  # type: config.AddLabelsAction

    def get_labels(self, item: Item) -> Set[str]:
        return {x.eval(item=item) for x in self.cfg.add_labels}

    async def __call__(self, item: Item) -> None:
        labels = self.get_labels(item)
        wanted = labels.union(item.labels)
        if len(wanted) == len(item.labels):
            return

        await item.refetch_info()
        wanted = labels.union(item.labels)
        if len(wanted) == len(item.labels):
            return
        await item.put_and_update({'labels': wanted})


class RemoveLabelsAction(Action):
    def __init__(self, cfg: config.RemoveLabelsAction, msgs: Iterable[str] = None) -> None:
        super().__init__(cfg, msgs)
        self._cfg = cfg  # Help jedi

    def get_labels(self, item: Item) -> Set[str]:
        return {x.eval(item=item) for x in self.cfg.remove_labels}

    async def __call__(self, item) -> None:
        labels = self.get_labels(item)
        wanted = set(item.labels) - labels
        if len(wanted) == len(item.labels):
            return

        await item.refetch_info()
        wanted = set(item.labels) - labels
        if len(wanted) == len(item.labels):
            return
        await item.put_and_update({'labels': wanted})


class AssignAction(Action):
    def __init__(self, cfg: config.AssignAction, msgs: Iterable[str] = None) -> None:
        super().__init__(cfg, msgs)
        self._cfg = cfg  # Help jedi

    def get_assignees(self, item: Item) -> Tuple[str, ...]:
        return tuple(x.eval(item=item) for x in self.cfg.assign)

    async def __call__(self, item: Item) -> None:
        existing = {x.username: x for x in item.assignees}
        wanted = {
            existing.get(x) or (await User.fetch_by_username(item.api, x))
            for x in self.get_assignees(item)
        }.union(existing.values())
        if len(existing) == len(wanted):
            return

        await item.refetch_info()
        existing = {x.username: x for x in item.assignees}
        wanted.update(existing.values())
        if len(existing) == len(wanted):
            return
        await item.put_and_update({'assignee_ids': sorted([x.id for x in wanted])})


class ReassignAction(Action):
    def __init__(self, cfg: config.ReassignAction, msgs: Iterable[str] = None) -> None:
        super().__init__(cfg, msgs)
        self._cfg = cfg  # Help jedi

    def get_assignees(self, item: Item) -> Tuple[str, ...]:
        return tuple(x.eval(item=item) for x in self.cfg.reassign)

    async def __call__(self, item: Item) -> None:
        existing = {x.username: x for x in item.assignees}
        wanted = {
            existing.get(x) or (await User.fetch_by_username(item.api, x))
            for x in self.get_assignees(item)
        }
        if not wanted.symmetric_difference(existing.values()):
            return

        await item.put_and_update({'assignee_ids': sorted([x.id for x in wanted])})


class CommentAction(Action):
    def __init__(self, cfg: config.CommentAction, msgs: Iterable[str] = None) -> None:
        super().__init__(cfg, msgs)
        self._cfg = cfg  # Help jedi

    def get_mention(self, item: Item) -> Tuple[str, ...]:
        return tuple(x.eval(item=item) for x in self.cfg.comment.mention)

    async def __call__(self, item: Item) -> None:
        mention = ' '.join(f'@{x}' for x in self.get_mention(item))
        msgs = '\n\n'.join(self.msgs)
        comment = f'{mention} {msgs}'
        await item.comment(comment)


ACTION_MAP = {
    config.AddLabelsAction: AddLabelsAction,
    config.RemoveLabelsAction: RemoveLabelsAction,
    config.AssignAction: AssignAction,
    config.ReassignAction: ReassignAction,
    config.CommentAction: CommentAction,
}  # type: Dict[Type[config.Action], Type[Action]]


class Rule:
    def __init__(self, cfg: config.Rule) -> None:
        self._cfg = cfg
        self._filter = parse_filter(self.cfg.filter) if self.cfg.filter else lambda item: True
        self._checks = [CHECK_MAP[type(cfg)](cfg) for cfg in cfg.checks or ()]
        self._actions = [(ACTION_MAP[type(cfg)], cfg) for cfg in cfg.actions]

    @property
    def cfg(self) -> config.Rule:
        return self._cfg

    @property
    def actions(self) -> List[Tuple[Type[Action], config.ActionUnion]]:
        return self._actions

    @property
    def filter(self) -> search.Function:
        return self._filter

    @property
    def checks(self) -> List[LabelSetCheck]:
        return self._checks

    async def run(self, item: Item) -> List[Action]:
        match = self.filter(item)
        if not match:
            return []

        msgs = [x for x in [check.run(item) for check in self.checks] if x]
        if self.checks and not msgs:
            return []

        actions = [Action(cfg, msgs) for Action, cfg in self.actions]
        return actions


class Checker(metaclass=ABCMeta):
    def __init__(self, cfg: config.CheckerUnion) -> None:
        self._cfg = cfg
        self._rules = tuple(Rule(cfg) for cfg in self.cfg.rules)

    @property
    def api(self) -> gitlab.Api:
        return gitlab.get_api(self.cfg.gitlab_url, self.cfg.api_token)

    @property
    def cfg(self):
        return self._cfg

    @property
    def rules(self) -> Tuple[Rule, ...]:
        return self._rules

    async def run(self) -> None:
        async for item in self._fetch():
            actions = [action for rule in self.rules for action in await rule.run(item)]
            for action in actions:
                # TODO: Actions that alter item.info could be accumulated, item.commit()?
                await action(item)

    @abstractmethod
    async def _fetch(self) -> AsyncGenerator[Item, None]:
        yield Issue(self.api, {})  # pragma: no cover


class ProjectIssueChecker(Checker):

    def __init__(self, cfg: config.CheckProjectIssues) -> None:
        super().__init__(cfg)

    async def _fetch(self) -> AsyncGenerator[Issue, None]:
        for item in await Issue.search_project(self.api, self.cfg.project_path):
            yield item


class GroupIssueChecker(Checker):

    def __init__(self, cfg: config.CheckGroupIssues) -> None:
        super().__init__(cfg)

    async def _fetch(self) -> AsyncGenerator[Issue, None]:
        for item in await Issue.search_group(self.api, self.cfg.group_path):
            yield item


class ProjectMergeRequestChecker(Checker):

    def __init__(self, cfg: config.CheckProjectMergeRequests) -> None:
        super().__init__(cfg)

    async def _fetch(self) -> AsyncGenerator[MergeRequest, None]:
        for item in await MergeRequest.search_project(self.api, self.cfg.project_path):
            yield item


class GroupMergeRequestChecker(Checker):

    def __init__(self, cfg: config.CheckGroupMergeRequests) -> None:
        super().__init__(cfg)

    async def _fetch(self) -> AsyncGenerator[MergeRequest, None]:
        for item in await MergeRequest.search_group(self.api, self.cfg.group_path):
            yield item
