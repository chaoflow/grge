# -*- coding: utf-8 -*-
#
# Copyright 2019  Ternaris.
# SPDX-License-Identifier: Apache-2.0

from enum import Enum, auto
from typing import Dict, Iterable, Sequence, Tuple

import commonmark


def _split_at_join(sequence: Sequence[str], idx: int) -> Tuple[str, str]:
    return ''.join(sequence[:idx]), ''.join(sequence[idx:])


def extract_preamble_and_rest(description: str) -> Tuple[str, str]:
    """The preamble is everything up to and excluding the first heading."""
    parser = commonmark.Parser()
    ast = parser.parse(description)
    for node in iterate_children(ast):
        if node.t == 'heading':
            lines = description.splitlines(keepends=True)
            preamble, rest = _split_at_join(lines, node.sourcepos[0][0] - 1)
            break
    else:
        preamble = description
        rest = ''
    return preamble, rest


def iterate_children(node: commonmark.node.Node) -> commonmark.node.Node:
    child = node.first_child
    while child:
        yield child
        child = child.nxt


class InvalidCharacter(Exception):
    pass


# pylint: disable=too-many-statements,too-many-branches
def parse_task_trailer(string: str) -> Dict[str, Tuple[str, ...]]:
    class State(Enum):
        START = auto()
        ASSIGNEE = auto()
        LABEL = auto()

    class Substate(Enum):
        START = auto()
        ALNUM = auto()
        QUOTED = auto()

    assignees = []
    labels = []
    idx = 0
    stack = []
    state = State.START
    substate = Substate.START
    while idx < len(string):
        char = string[idx]  # type: str

        if state == State.START:
            if char == '@':
                state = State.ASSIGNEE

            elif char == '~':
                state = State.LABEL

            elif char == ' ':
                pass

            else:
                raise InvalidCharacter(string[idx], idx + 1, string)

        elif state == State.ASSIGNEE:
            done = False
            if char.isalnum() or char in '.-_':
                stack.append(char)
            elif char == ' ':
                done = True
            else:
                raise InvalidCharacter(string[idx], idx + 1, string)

            if done or idx == len(string) - 1:
                assignees.append(''.join(stack))
                stack.clear()
                state = State.START

        elif state == State.LABEL:
            if substate == Substate.START:
                if char == '"':
                    substate = Substate.QUOTED
                elif char.isalnum():
                    stack.append(char)
                    substate = Substate.ALNUM
                else:
                    raise InvalidCharacter(string[idx], idx + 1, string)

            elif substate == Substate.ALNUM:
                done = False
                if char.isalnum():
                    stack.append(char)
                elif char == ' ':
                    done = True
                else:
                    raise InvalidCharacter(string[idx], idx + 1, string)

                if done or idx == len(string) - 1:
                    labels.append(''.join(stack))
                    stack.clear()
                    state = State.START
                    substate = Substate.START

            elif substate == Substate.QUOTED:
                if char == '"':
                    labels.append(''.join(stack))
                    stack.clear()
                    state = State.START
                    substate = Substate.START
                else:
                    stack.append(char)

            else:
                raise RuntimeError(f'Unknown subtate {substate}.')  # pragma: no cover

        else:
            raise RuntimeError(f'Unknown state {state}.')  # pragma: no cover

        idx += 1
        # print(idx, state, substate, stack)

    assert not stack, ''.join(stack)
    assert state == State.START, state
    assert substate == Substate.START, substate

    return {'assignees': tuple(assignees), 'labels': tuple(labels)}


def join_labels(labels: Iterable[str]) -> str:
    return ' '.join(f'~{x}' for x in sorted(labels))
