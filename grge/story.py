# -*- coding: utf-8 -*-
#
# Copyright 2018 - 2019  Ternaris.
# SPDX-License-Identifier: Apache-2.0

import logging as log

from .issue import Issue
from .task import Task, TaskIssue
from .user import User


class StoryIssue(Issue):
    active_story_label = 'active'
    active_task_label = 'active'
    task_project = None
    story_project = None
    task_labels = ('task',)

    @property
    def tasks(self):
        assert self.task_project
        assert self.story_project
        same_namespace = (self.task_project.path_with_namespace.rsplit('/', 1)[0] ==
                          self.story_project.path_with_namespace.rsplit('/', 1)[0])
        project_name = self.task_project.path_with_namespace.rsplit('/', 1)[1]
        tasks = tuple(
            x.replace(story=self) for x in Task.extract_all(self.description)
            if (x.project_path == self.task_project.path_with_namespace or
                same_namespace and x.project_path == project_name or
                x.project_path is None and x.iid is None or
                x.project_path is None and not self.story_and_task_project_differ)
        )
        log.debug('Extracted tasks: %r', tasks)
        return tasks

    @property
    def story_and_task_project_differ(self):
        return self.task_project.id != self.story_project.id

    def _is_active(self, issue):
        attrname = {
            StoryIssue: 'active_story_label',
            TaskIssue: 'active_task_label',
        }[type(issue)]
        label = getattr(self, attrname)
        return label in issue.labels

    async def synchronize_tasks(self, task_issues):
        story = self
        changed_tasks = []
        for task in story.tasks:
            _changed_task = await self._synchronize_task(task, task_issues)
            if _changed_task:
                changed_tasks.append(_changed_task)

        if changed_tasks:
            lines = story.info['description'].splitlines()
            for task in changed_tasks:
                row_idx = task.sourcepos[0][0] - 1
                col_idx = task.sourcepos[0][1] - 1
                lines[row_idx] = lines[row_idx][:col_idx] + task.render()

            # Minimize possibility of race condition
            old_description = self.description
            await story.refetch_info()
            if old_description != self.description:
                return  # Abort mission, will be handled in next run
            await story.put_and_update({'description': '\n'.join(lines)})

    @property
    def story_reference(self):
        story_namespace, story_pname = self.story_project.path_with_namespace.rsplit('/', 1)
        if self.story_and_task_project_differ:
            if story_namespace == self.task_project.path_with_namespace.rsplit('/', 1)[0]:
                return f'{story_pname}#{self.iid}'
            return f'{self.story_project.path_with_namespace}#{self.iid}'
        return f'#{self.iid}'

    async def _synchronize_task(self, task, task_issues):
        """Synchronize task, returning changed task, if any."""
        task_namespace, task_project_name = self.task_project.path_with_namespace.rsplit('/', 1)
        if task_namespace == self.story_project.path_with_namespace.rsplit('/', 1)[0]:
            project_path = task_project_name
        else:
            project_path = self.task_project.path_with_namespace

        task_issue = self._find_task_issue(task, task_issues)
        if task_issue is False:
            return False

        active = self._is_active(self)
        if active and not task_issue and not task.done:
            if self.story_and_task_project_differ:
                task = task.replace(project_path=project_path)
            _labels = (self.active_task_label,) + self.task_labels + task.labels
            info = {
                'title': f'{task.title} - {self.story_reference} {self.title}',
                'labels': _labels,
            }
            if task.assignees:
                info['assignee_ids'] = [
                    (await User.fetch_by_username(self.api, x)).id for x in task.assignees
                ]
            task_issue = await TaskIssue.create(self.task_project.api, self.task_project.id, info)
            return task.replace(iid=task_issue.iid)

        if not task_issue:
            return None

        task_issue_changes = self._get_task_issue_changes(active, task_issue)
        if task_issue_changes:
            await task_issue.put_and_update(task_issue_changes)
        task_changes = self._get_task_changes(active, task, task_issue)
        if self.story_and_task_project_differ and task.project_path != project_path:
            task_changes['project_path'] = project_path
        return task.replace(**task_changes) if task_changes else None

    def _find_task_issue(self, task, task_issues):
        if task.iid:
            task_issue = task_issues.get(task.iid)
            if not task_issue:
                log.warning('Skipping vanished task #%s', task.iid)
                return False
            return task_issue

        # Look for task issues we might have created and got
        # interrupted when updating the story's task list.
        titles = [f'{task.title} - {self.story_reference} {self.title}']
        if self.story_and_task_project_differ:
            titles.append(
                f'{task.title} - {self.story_project.path_with_namespace}#{self.iid} {self.title}'
            )
        matches = (x for x in task_issues.values() if x.title in titles)
        task_issue = next(matches, None)
        return task_issue

    def _get_task_changes(self, active, task, task_issue):
        changes = {}
        if not task.iid:
            changes['iid'] = task_issue.iid

        if active:
            done = task_issue.state == 'closed'
            if bool(done) ^ bool(task.done):
                changes['done'] = done

        task_title = task_issue.title.split(f' - {self.story_reference} ', 1)[0]
        task_title = task_title.split('@', 1)[0].strip()  # recover from failure case
        if task.title != task_title:
            changes['title'] = task_title

        assignees = tuple(sorted(x.username for x in task_issue.assignees))
        if assignees != task.assignees:
            changes['assignees'] = assignees

        special_labels = self.task_labels + (self.active_task_label,)
        labels = tuple(sorted(set(task_issue.labels).difference(special_labels)))
        if labels != task.labels:
            changes['labels'] = labels

        return changes

    def _get_task_issue_changes(self, active, task_issue):
        changes = {}
        if self._is_active(task_issue) != active:
            _labels = set(task_issue.labels)
            getattr(_labels, 'add' if active else 'remove')(self.active_task_label)
            changes['labels'] = tuple(sorted(_labels))

        if f'- {self.story_reference} ' not in task_issue.title:
            full_reference = f'{self.story_project.path_with_namespace}#{self.iid}'
            task_title = task_issue.title.split(f' - {full_reference} ', 1)[0]
            changes['title'] = f'{task_title} - {self.story_reference} {self.title}'

        return changes
