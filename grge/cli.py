# -*- coding: utf-8 -*-
#
# Copyright 2018 - 2019  Ternaris.
# SPDX-License-Identifier: Apache-2.0

import os
import logging

import click

from . import config
from .daemon import Daemon
from .utils import asyncio_run, launch_pdb_on_exception


LOGLEVELS = [
    'CRITICAL',  # 50
    'ERROR',  # 40
    'WARNING',  # 30
    'INFO',  # 20
    'DEBUG',  # 10
]


@click.group()
def grge():
    """Gitlab Rocket GEar"""


@grge.command('daemon')
@click.argument('config_file', type=click.Path(exists=True, dir_okay=False))
@click.option(
    '--loglevel', type=click.Choice(LOGLEVELS), default='INFO', show_default=True,
    callback=lambda ctx, param, value: getattr(logging, value), help='Set loglevel.'
)
@click.option('--once', is_flag=True, help='Perform only one synchronization loop then exit.')
@click.option('--dry-run', is_flag=True, help='Print what would happen then exit.')
@asyncio_run
async def grge_daemon(config_file, once, dry_run, loglevel):
    """Gitlab Rocket GEar Daemon."""
    once = once or dry_run

    # Initialise logging, silencing urllib3
    logging.getLogger('urllib3').setLevel(logging.WARNING)
    logging.getLogger().setLevel(loglevel)
    logging.info('Powering up')

    daemon = Daemon(config.load(config_file))
    daemon.install_signal_handlers()
    await daemon.run(once=once, dry_run=dry_run)


def cli():
    """Setuptools entrypoint"""
    with launch_pdb_on_exception(os.environ.get('PDB')):
        grge(auto_envvar_prefix='GRGE')  # pylint: disable=unexpected-keyword-arg
