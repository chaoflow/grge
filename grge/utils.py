# -*- coding: utf-8 -*-
#
# Copyright 2017 - 2019  Ternaris.
# SPDX-License-Identifier: Apache-2.0

"""Utility functions"""

import asyncio
from contextlib import contextmanager
from functools import update_wrapper
from typing import Callable, List


def asyncio_run(func):
    """Wrap async function to run synchronously.

    This is useful to implement click commands using async functions.
    """
    def sync_func(*args, **kw):
        try:
            return asyncio.run(func(*args, **kw))
        except asyncio.CancelledError:
            print('Stopped.')
    return update_wrapper(sync_func, func)


@contextmanager
def launch_pdb_on_exception(launch=True):
    """Return contextmanager launching pdb upon exception.

    Use like this, to toggle via env variable:

    with launch_pdb_on_exception(os.environ.get('PDB')):
        cli()
    """
    # pylint: disable=broad-except
    try:
        yield
    except Exception:  # noqa
        if launch:
            import pdb
            pdb.xpm()  # pylint: disable=no-member
        else:
            raise


def popwhile(pred: Callable, lst: List) -> List:
    """Pop items from list while pred(lst[-1]) is True"""
    arg = None
    args: List = []
    while lst and pred(lst[-1]):
        arg = lst.pop()
        args.insert(0, arg)
    return args
