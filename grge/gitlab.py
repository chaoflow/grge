# -*- coding: utf-8 -*-
#
# Copyright 2019  Ternaris.
# SPDX-License-Identifier: Apache-2.0
#
# This module contains code originally published
# Copyright 2017 Smarkets Limited
# SPDX-License-Identifier: BSD-3-Clause

from dataclasses import dataclass, field, replace
from typing import Any, Callable, Dict, List, Optional, Tuple, Union, cast

import aiohttp
import ujson
from yarl import URL


APIS = {}  # type: Dict[Tuple[str, str], 'Api']


def get_api(gitlab_url: str, auth_token: str) -> 'Api':
    key = (gitlab_url, auth_token)
    api = APIS.get(key)
    if api is None:
        APIS[key] = api = Api(*key)
    return api


class Api:
    _session = None

    def __init__(self, gitlab_url: str, auth_token: str) -> None:
        self._api_base_url = URL(gitlab_url) / 'api/v4'
        self._auth_token = auth_token

    async def session(self) -> aiohttp.ClientSession:
        if self._session is None:
            headers = {'PRIVATE-TOKEN': self._auth_token}
            self._session = aiohttp.ClientSession(headers=headers, json_serialize=ujson.dumps)
        return self._session

    async def call(self, command: 'Command', sudo: Optional[Union[str, int]] = None):
        url = self._api_base_url / command.endpoint.lstrip('/')  # TODO: make all api calls relative
        method = command.method(await self.session())
        headers = {}
        if sudo is not None:
            headers['SUDO'] = f'{sudo}'  # numeric id or username

        async with method(url, headers=headers, **command.call_args) as response:
            if response.status == 202:
                return True  # Accepted

            if response.status == 204:
                return True  # NoContent

            if response.status < 300:
                data = await response.json(loads=ujson.loads)
                return command.extract(data) if command.extract else data

            if response.status == 304:
                return False  # Not Modified

            errors = {
                400: BadRequest,
                401: Unauthorized,
                403: Forbidden,
                404: NotFound,
                405: MethodNotAllowed,
                406: NotAcceptable,
                409: Conflict,
                422: Unprocessable,
                500: InternalServerError,
            }

            def other_error(code: int, url: URL, msg: Any):
                exception = InternalServerError if 500 < code < 600 else UnexpectedError
                return exception(code, url, msg)

            error = errors.get(response.status, other_error)
            try:
                err_message = await response.json(loads=ujson.loads)
            except ValueError:
                err_message = response.reason

            raise error(response.status, url, err_message)

    async def collect_all_pages(self, get_command: 'GET') -> List[Dict[str, Any]]:
        result = []  # type: List[Dict[str, Any]]
        fetch_again, page_no = True, 1
        while fetch_again:
            page = await self.call(get_command.for_page(page_no))
            if page:
                result.extend(page)
                page_no += 1
            else:
                fetch_again = False

        return result

    async def version(self) -> 'Version':
        response = await self.call(GET('/version'))
        return Version.parse(response['version'])


def from_singleton_list(fun=None):
    fun = fun or (lambda x: x)

    def extractor(response_list):
        assert isinstance(response_list, list), type(response_list)
        assert len(response_list) <= 1, len(response_list)
        if not response_list:
            return None
        return fun(response_list[0])

    return extractor


@dataclass(frozen=True)
class Command:
    endpoint: str
    args: Optional[Dict] = field(default_factory=dict)
    extract: Optional[Callable] = None

    @property
    def call_args(self):
        return {'json': self.args}

    def method(self, session: aiohttp.ClientSession) -> Any:
        return getattr(session, type(self).__name__.lower())

    def replace(self, **kw) -> 'Command':
        return replace(self, **kw)


class GET(Command):
    @property
    def call_args(self):
        return {'params': _prepare_params(self.args)}

    def for_page(self, page_no: int) -> 'GET':
        newargs = dict(self.args or (), page=page_no, per_page=100)
        return cast('GET', self.replace(args=newargs))


class PUT(Command):
    pass


class POST(Command):
    pass


class DELETE(Command):
    pass


def _prepare_params(params):
    def process(val):
        if isinstance(val, bool):
            return ('true' if val else 'false', False)
        if isinstance(val, (list, tuple)):
            return ([process(x)[0] for x in val], True)
        return val, False

    lst = []
    for key, val in (params or {}).items():
        val, is_list = process(val)
        if is_list:
            lst.extend((key, x) for x in val)
        else:
            lst.append((key, val))
    return lst


class ApiError(Exception):
    @property
    def error_message(self):
        args = self.args
        if len(args) != 2:
            return None

        arg = args[1]  # pylint: disable=unsubscriptable-object
        if isinstance(arg, dict):
            return arg.get('message')
        return arg


class BadRequest(ApiError):
    pass


class Unauthorized(ApiError):
    pass


class Forbidden(ApiError):
    pass


class NotFound(ApiError):
    pass


class MethodNotAllowed(ApiError):
    pass


class NotAcceptable(ApiError):
    pass


class Conflict(ApiError):
    pass


class Unprocessable(ApiError):
    pass


class InternalServerError(ApiError):
    pass


class UnexpectedError(ApiError):
    pass


class Resource:
    def __init__(self, api: Api, info: Dict[str, Any]):
        self._info = info
        self._api = api

    @property
    def info(self) -> Dict[str, Any]:
        return self._info

    @property
    def id(self) -> int:  # pylint: disable=invalid-name
        return self.info['id']

    @property
    def api(self) -> Api:
        return self._api

    def __repr__(self) -> str:
        return '{0.__class__.__name__}({0._api}, {0.info})'.format(self)


class StandaloneResource(Resource):
    def __eq__(self, other) -> bool:
        return self._key() == other._key()  # pylint: disable=protected-access

    def __hash__(self) -> int:
        return hash(self._key())

    def _key(self) -> Tuple[str, str, int]:
        return (self.__class__.__module__, self.__class__.__name__, self.id)


@dataclass(frozen=True)
class Version:
    release: Tuple[int, ...]
    edition: Optional[str] = None

    @classmethod
    def parse(cls, string: str) -> 'Version':
        edition: Optional[str]
        maybe_split_string = string.split('-', maxsplit=1)
        if len(maybe_split_string) == 2:
            release_string, edition = maybe_split_string
        else:
            release_string, edition = string, None

        release = tuple(int(number) for number in release_string.split('.'))
        return cls(release=release, edition=edition)

    @property
    def is_ee(self) -> bool:
        return self.edition == 'ee'

    def __str__(self) -> str:
        return '%s-%s' % ('.'.join(map(str, self.release)), self.edition)
