# -*- coding: utf-8 -*-
#
# Copyright 2019  Ternaris.
# SPDX-License-Identifier: Apache-2.0

from dataclasses import asdict, dataclass, field
from functools import partial
from typing import Dict, List, Optional, Union

import aiofiles
import dacite
from ruamel import yaml


# TODO: Validation: Label names must not contain commas
# TODO: Validation: provide useful information in case of config errors


async def load(path):
    async with aiofiles.open(path) as f:
        config = yaml.safe_load(await f.read())
    return Config.from_dict(config)


class FString(str):
    """An expression to be used in a python f-string.

    The evaluation context of the f-string depends on where in the
    configuration it is used. See :class:`.Rule` for an example.
    """
    def eval(self, **kw):
        if '{' not in self:
            return self
        return eval(f"f'{self}'", None, kw)  # pylint: disable=eval-used


class FilterString(str):
    """A string defining a filter to apply on issues and merge requests.

    TODO: Document all filters here or point to documentation.
    """


@dataclass(frozen=True)
class _Base:
    def asdict(self):
        """Convenience wrapper for dataclasses.asdict()."""
        return asdict(self)

    @classmethod
    def from_dict(cls, dct):
        cfg = dacite.from_dict(cls, dct, config=dacite.Config(
            strict=True,
            type_hooks={
                FString: FString,
                FilterString: FilterString,
            }
        ))
        return cfg


@dataclass(frozen=True)
class GitlabGroup:
    """A GitLab group."""

    gitlab_url: str
    """URL to the GitLab instance."""

    api_token: str
    """API token to access group with."""

    group_path: str
    """Full path to group."""


@dataclass(frozen=True)
class GitlabProject:
    """A GitLab project."""

    gitlab_url: str
    """URL to the GitLab instance."""

    api_token: str
    """API token to access project with."""

    project_path: str
    """Full path to project."""


@dataclass(frozen=True)
class Action(_Base):
    """An Action to be applied to an issue or merge request."""


@dataclass(frozen=True)
class AddLabelsAction(Action):
    """Add labels to issue or merge request."""
    add_labels: List[FString]


@dataclass(frozen=True)
class RemoveLabelsAction(Action):
    remove_labels: List[FString]


@dataclass(frozen=True)
class AssignAction(Action):
    assign: List[FString]


@dataclass(frozen=True)
class ReassignAction(Action):
    reassign: List[FString]


@dataclass(frozen=True)
class Comment:
    mention: List[FString]


@dataclass(frozen=True)
class CommentAction(Action):
    comment: Comment


ActionUnion = Union[
    AddLabelsAction,
    RemoveLabelsAction,
    AssignAction,
    ReassignAction,
    CommentAction,
]


@dataclass(frozen=True)
class LabelSetCheck(_Base):
    """Check item for a set of labels."""

    label_set: List[str]
    """List of mutually exclusive labels."""

    required: bool = True
    """Indicate whether a label of this set is required."""


CheckUnion = Union[
    LabelSetCheck,
]


@dataclass(frozen=True)
class Rule(_Base):
    """Rules to be applied on issues or merge requests."""

    actions: List[ActionUnion]
    filter: Optional[FilterString]
    checks: Optional[List[CheckUnion]]


@dataclass(frozen=True)
class Checker(_Base):
    rules: List[Rule]
    """List of :class:`.Rule` to check issues and/or merge requests for."""


@dataclass(frozen=True)
class CheckGroupIssues(Checker, GitlabGroup):
    pass


@dataclass(frozen=True)
class CheckGroupMergeRequests(Checker, GitlabGroup):
    pass


@dataclass(frozen=True)
class CheckProjectIssues(Checker, GitlabProject):
    pass


@dataclass(frozen=True)
class CheckProjectMergeRequests(Checker, GitlabProject):
    pass


CheckerUnion = Union[
    CheckGroupIssues,
    CheckGroupMergeRequests,
    CheckProjectIssues,
    CheckProjectMergeRequests,
]


@dataclass(frozen=True)
class ProjectSyncSource(GitlabProject):
    """A project to sync issues from.

    Currently, the inherited attributes are not displayed, please see
    also :class:`.GitlabProject`.
    """

    static_labels: List[str] = field(default_factory=list)
    """List of labels to be always added to target issues linking to this source."""

    label_map: Dict[str, str] = field(default_factory=dict)
    """A dictionary mapping target label names (keys) to source label names (values).

    The source is authoritative for these labels. Changes to these
    labels on the target will be gone after the next sync.
    """


@dataclass(frozen=True)
class ProjectSyncTarget(GitlabProject):
    """A project to sync issues to.

    Currently, the inherited attributes are not displayed, please see
    also :class:`.GitlabProject`.
    """

    managed_label: str = 'managed'
    """Label indicating that a target issue is managed by GRGE."""


@dataclass(frozen=True)
class SyncExternalIssues(_Base):
    """Instructions to sync issues from source projects to a target project."""

    sources: List[ProjectSyncSource]
    """One or more :class:`.ProjectSyncSource` to fetch issues from"""

    target: ProjectSyncTarget
    """A :class:`.ProjectSyncTarget` to create issues in."""


@dataclass(frozen=True)
class SyncStoriesAndTasks(_Base):
    gitlab_url: str
    """URL to the GitLab instance."""

    api_token: str
    """API token to access project with."""

    project_path: str
    """Path to project where stories are looked for."""

    task_project_path: Optional[str]
    """Optionally, place tasks into different project.

    Normally they live next to the story issues.
    """

    active_story_label: str = 'active'
    """Optionally, specify label that indicates an active story (default: 'active')."""

    active_task_label: str = 'active'
    """Optionally, specify label that indicates an active task (default: 'active')."""

    story_labels: List[str] = field(default_factory=partial(list, ['story']))  # type: ignore
    """Labels that define a story, used to search for stories (default: ['story'])."""

    task_labels: List[str] = field(default_factory=partial(list, ['task']))  # type: ignore
    """Labels that define a task, used to search for and create tasks (default: ['task'])."""


@dataclass(frozen=True)
class Job(_Base):
    name: Optional[str]
    sleep_interval: int


@dataclass(frozen=True)
class CheckGroupIssuesJob(Job):
    check_group_issues: CheckGroupIssues


@dataclass(frozen=True)
class CheckGroupMergeRequestsJob(Job):
    check_group_merge_requests: CheckGroupMergeRequests


@dataclass(frozen=True)
class CheckProjectIssuesJob(Job):
    check_project_issues: CheckProjectIssues


@dataclass(frozen=True)
class CheckProjectMergeRequestsJob(Job):
    check_project_merge_requests: CheckProjectMergeRequests


@dataclass(frozen=True)
class SyncExternalIssuesJob(Job):
    sync_external_issues: SyncExternalIssues


@dataclass(frozen=True)
class SyncStoriesAndTasksJob(Job):
    sync_stories_and_tasks: SyncStoriesAndTasks


JobUnion = Union[
    CheckGroupIssuesJob,
    CheckGroupMergeRequestsJob,
    CheckProjectIssuesJob,
    CheckProjectMergeRequestsJob,
    SyncExternalIssuesJob,
    SyncStoriesAndTasksJob,
]


@dataclass(frozen=True)
class Config(_Base):
    jobs: List[JobUnion]
