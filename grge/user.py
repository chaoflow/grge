# -*- coding: utf-8 -*-
#
# Copyright 2019  Ternaris.
# SPDX-License-Identifier: Apache-2.0
#
# This module contains code originally published
# Copyright 2017 Smarkets Limited
# SPDX-License-Identifier: BSD-3-Clause

from . import gitlab


GET = gitlab.GET


class User(gitlab.StandaloneResource):

    def __str__(self):
        return self.username

    @classmethod
    async def myself(cls, api):
        info = await api.call(GET('/user'))

        if info.get('is_admin') is None:  # WORKAROUND FOR BUG IN 9.2.2
            try:
                # sudoing succeeds iff we are admin
                await api.call(GET('/user'), sudo=info['id'])
                info['is_admin'] = True
            except gitlab.Forbidden:
                info['is_admin'] = False

        return cls(api, info)

    @property
    def is_admin(self):
        return self.info['is_admin']

    @classmethod
    async def fetch_by_id(cls, api, user_id):
        info = await api.call(GET('/users/%s' % user_id))
        return cls(api, info)

    @classmethod
    async def fetch_by_username(cls, api, username):
        info = await api.call(GET(
            '/users',
            {'username': username},
            gitlab.from_singleton_list(),
        ))
        return cls(api, info)

    @property
    def avatar_url(self):
        return self.info['avatar_url']

    @property
    def name(self):
        return self.info['name'].strip()

    @property
    def username(self):
        return self.info['username']

    @property
    def email(self):
        """Only visible to admins and 'self'. Sigh."""
        return self.info.get('email')

    @property
    def state(self):
        return self.info['state']

    @property
    def web_url(self):
        return self.info['web_url']
