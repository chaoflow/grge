with import <nixpkgs> {};

let
    python = python37;

in

stdenv.mkDerivation rec {
  name = "grge";
  shellHook = ''
    export VENV=".venv"
    export XDG_CACHE_HOME=.cache
    unset SOURCE_DATE_EPOCH
    test -d "$VENV" || (${python}/bin/python -m venv "$VENV"
      source "$VENV/bin/activate"
      pip install -r requirements/develop.txt
      pip install -e .
    )
    source "$VENV/bin/activate"
    eval "$(_GRGE_COMPLETE=source grge)"
  '';
  buildInputs = [
    libffi
    python
  ];
}
